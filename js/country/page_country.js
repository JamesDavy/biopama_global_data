 //test URL
 //https://dopa-services.jrc.ec.europa.eu/services/d6dopa40/administrative_units/get_country_all_inds?format=json&c_un_m49=516
 
 /* 
 all cards and data are found in drupalSettings.dataCardsCountry
 
 console.log(drupalSettings.dataCardsCountry ); //to find your data
 */
 //console.log(drupalSettings.dataCardsCountry );
(function ($, Drupal) {
	
	//this is a quick dump of the ACP countries, as I'm not going to lookup the node pages if I don't have to
	var countries = [{ iso_code: "ST" , name: "Sao Tome and Principe"}, { iso_code: "UG" , name: "Uganda"}, { iso_code: "TL" , name: "Timor-Leste"}, { iso_code: "GA" , name: "Gabon"}, { iso_code: "SZ" , name: "Eswatini"}, { iso_code: "TD" , name: "Chad"}, { iso_code: "BS" , name: "Bahamas"}, { iso_code: "TZ" , name: "Tanzania, United Republic of" }, { iso_code: "MH" , name: "Marshall Islands" }, { iso_code: "MU" , name: "Mauritius" }, { iso_code: "GQ" , name: "Equatorial Guinea" }, { iso_code: "TO" , name: "Tonga" }, { iso_code: "CG" , name: "Congo" }, { iso_code: "PW" , name: "Palau" }, { iso_code: "MW" , name: "Malawi" }, { iso_code: "BI" , name: "Burundi" }, { iso_code: "AO" , name: "Angola" }, { iso_code: "ET" , name: "Ethiopia" }, { iso_code: "SO" , name: "Somalia" }, { iso_code: "FJ" , name: "Fiji" }, { iso_code: "LS" , name: "Lesotho" }, { iso_code: "GW" , name: "Guinea-Bissau" }, { iso_code: "ZW" , name: "Zimbabwe" }, { iso_code: "DO" , name: "Dominican Republic" }, { iso_code: "TT" , name: "Trinidad and Tobago" }, { iso_code: "CI" , name: "Côte d'Ivoire" }, { iso_code: "CV" , name: "Cabo Verde" }, { iso_code: "SS" , name: "South Sudan" }, { iso_code: "DM" , name: "Dominica" }, { iso_code: "BZ" , name: "Belize" }, { iso_code: "NA" , name: "Namibia" }, { iso_code: "GY" , name: "Guyana" }, { iso_code: "ER" , name: "Eritrea" }, { iso_code: "KI" , name: "Kiribati" }, { iso_code: "CM" , name: "Cameroon" }, { iso_code: "HT" , name: "Haiti" }, { iso_code: "NE" , name: "Niger" }, { iso_code: "LR" , name: "Liberia" }, { iso_code: "GD" , name: "Grenada" }, { iso_code: "KM" , name: "Comoros" }, { iso_code: "SB" , name: "Solomon Islands" }, { iso_code: "AG" , name: "Antigua and Barbuda" }, { iso_code: "NG" , name: "Nigeria" }, { iso_code: "NR" , name: "Nauru" }, { iso_code: "VU" , name: "Vanuatu" }, { iso_code: "MG" , name: "Madagascar" }, { iso_code: "RW" , name: "Rwanda" }, { iso_code: "PG" , name: "Papua New Guinea" }, { iso_code: "MZ" , name: "Mozambique" }, { iso_code: "LC" , name: "Saint Lucia" }, { iso_code: "ZA" , name: "South Africa" }, { iso_code: "KE" , name: "Kenya" }, { iso_code: "GN" , name: "Guinea" }, { iso_code: "SR" , name: "Suriname" }, { iso_code: "CK" , name: "Cook Islands" }, { iso_code: "CF" , name: "Central African Republic" }, { iso_code: "BJ" , name: "Benin" }, { iso_code: "GH" , name: "Ghana" }, { iso_code: "CD" , name: "Congo (Democratic Republic of the)" }, { iso_code: "GM" , name: "Gambia" }, { iso_code: "SN" , name: "Senegal" }, { iso_code: "SC" , name: "Seychelles" }, { iso_code: "ZM" , name: "Zambia" }, { iso_code: "MR" , name: "Mauritania" }, { iso_code: "BF" , name: "Burkina Faso" }, { iso_code: "FM" , name: "Micronesia (Federated States of)" }, { iso_code: "TV" , name: "Tuvalu" }, { iso_code: "VC" , name: "Saint Vincent and the Grenadines" }, { iso_code: "CU" , name: "Cuba" }, { iso_code: "ML" , name: "Mali" }, { iso_code: "DJ" , name: "Djibouti" }, { iso_code: "SL" , name: "Sierra Leone" }, { iso_code: "SD" , name: "Sudan" }, { iso_code: "BB" , name: "Barbados" }, { iso_code: "JM" , name: "Jamaica" }, { iso_code: "KN" , name: "Saint Kitts and Nevis" }, { iso_code: "WS" , name: "Samoa" }, { iso_code: "BW" , name: "Botswana" }, { iso_code: "TG" , name: "Togo" }, { iso_code: "NU" , name: "Niue"}]
 
	var pathArray = window.location.pathname.split('/');
	var pathIsoCode = pathArray[pathArray.length - 1]; //gets the last thing in the URL (should be a country code)
	//var currentCountry.iso2 = pathIsoCode.toUpperCase(); //coverts it to upper case as the REST needs upper case
	var currentCountry = {
		name: '',
		iso2: pathIsoCode.toUpperCase(), //coverts it to upper case as the REST needs upper case
		iso3: '',
		un: '',
	}
	var countryChaged = false;
	var allCountryInds = []; //Country Indicators
	var allCountryPaStats = []; //Protected Area Indicators
	var countryChanged = 0;
	 
	getIndicatorData(currentCountry.iso2); //We run the big indicator call right away and try to get all our data in the background. 
	function zoomToCountry(iso2){
		var result;
		if(iso2 === 'FJ'){
			mymap.fitBounds([[166.61,-26.39], [192.01,-9.62]]);
		} else if (iso2 === 'TV'){
			mymap.fitBounds([[168.58,-13.60], [191.48,-3.88]]);
		} else if (iso2 === 'KI'){
			mymap.fitBounds([[-201.57,-15.70], [-136.18,10.31]]);
		} else {
			jQuery.ajax({
				url: DOPAgetCountryExtent+iso2,
				dataType: 'json',
				success: function(d) {
					mymap.fitBounds(jQuery.parseJSON(d.records[0].extent));
				},
				error: function() {
					console.log("Something is wrong with the REST servce for country bounds")
				}
			});
		}
	}
	function getIndicatorData(isoCode){
		var allCountryIndicatorsURL = DopaBaseUrl+'administrative_units/get_country_all_inds?format=json&b_iso2='+isoCode;
		var allCountryPaStatsURL = DopaBaseUrl+'administrative_units/get_country_pa_stats?format=json&b_iso2='+isoCode;
		$('.indicatorProccessed').removeClass('indicatorProccessed');

		$.when(
			$.getJSON(allCountryIndicatorsURL,function(d){
				allCountryInds = d['records'][0];
				currentCountry.name = allCountryInds.country_name; 
				currentCountry.iso2 = allCountryInds.country_iso2;
				currentCountry.iso3 = allCountryInds.country_iso3;
				currentCountry.un = allCountryInds.country_un_m49;
				if (mymap.getLayer("countrySelected")) {
					mymap.setLayoutProperty("countrySelected", 'visibility', 'visible');
					mymap.setFilter('countrySelected', ['==', 'iso3', currentCountry.iso3]);	
				}
				countryChanged = 1;
				$('.country-name').text(currentCountry.name);
				$('#country-flag').attr("src", '/sites/default/files/country_flags/' + currentCountry.iso2.toLowerCase() + '.png');
				history.pushState({ 
					id: 'country_page'
				}, 'DOPA Country Data | ' + currentCountry.name, '/global_datasets/country/' + currentCountry.iso2.toLowerCase()); 
			}),
			$.getJSON(allCountryPaStatsURL,function(d){
				allCountryPaStats = d['records']; 
			})
		).then(function() {
			zoomToCountry(isoCode);
			$( ".wrapper-data-card.show" ).each(function() {  
				$( this ).trigger("shown.bs.collapse"); 
			});
		});
	}
	mymap.on('moveend', function () {
		//this is really just for when the page first loads
		if (countryChanged == 1){	
			updateCountry();
		}
	});
	function updateCountry(){ 
		if (mymap.getLayer("countrySelected")) {
			mymap.setLayoutProperty("countrySelected", 'visibility', 'visible');
			mymap.setFilter('countrySelected', ['==', 'iso3', currentCountry.iso3]);	
		}
		countryChanged = 0;
	};
	window.addEventListener('popstate', function (event) {
		var pathArray = window.location.pathname.split('/');
		var pathIsoCode = pathArray[pathArray.length - 1]; //gets the last thing in the URL (should be a country code)
		currentCountry.iso2 = pathIsoCode.toUpperCase(); //coverts it to upper case as the REST needs upper case
		getIndicatorData(currentCountry.iso2);
	}, false);
    Drupal.behaviors.biopamaDopa = {
		attach: function (context, settings) {
		    $(window).once().on('load scroll', function () {
				
				var countryIndicatorData;
				
				$("#country-search").chosen({
					disable_search_threshold: 5,
					width: "100%"
				}); 

				Object.keys(countries).forEach((item, index, arr) => {
				  	$("#country-search").append('<option value="' + countries[item].iso_code + '">' + countries[item].name + '</option>')
				  	if(!arr[index + 1]){
						//since I didn't make my object above alphabetical, and I'm too lazy to update it now, everyone has to sort it themselves.
						var countryOptions = $("#country-search option");
						countryOptions.sort(function(a,b) {
							if (a.text > b.text) return 1;
							else if (a.text < b.text) return -1;
							else return 0
						})
						$("#country-search").empty().append(countryOptions);
						$("#country-search").trigger("chosen:updated"); 
				  	}
				});
				
				$("#country-search").chosen().change(function(){
					getIndicatorData($(this).val());
				});
				
				$( ".report-icon" ).click(function( event ) {
				  	$(this).toggleClass("report-added");
					var reportCardID = $(this).closest('.card-text').prev().attr("data-target");
					var reportCardPart = 'Chart';
					if ( $(this).next().hasClass('wrapper-dopa-data-table') ) {
						reportCardPart = 'Table';
					}
					var indicator = reportCardID.replace("#collapse", "");
					if ($(this).hasClass("report-added")){
						$().addIndicatorToReport(indicator, reportCardPart);
					} else {
						$().removeIndicatorFromReport(indicator, reportCardPart); 
					}
				});
				
				// #### Card Start  - we go through every card and attach the functions needed to generate the content
				/* 
				## Start Profile
				*/
				$('#collapseProfile').on('shown.bs.collapse', function () {
					//this card is a custom one to update the URLS's of all the country links
					$(this).find("a.link-iso2").each(function( i ) {
						var linkHref = $(this).attr("href");
						$(this).attr("href", linkHref.slice(0,-2) + currentCountry.iso2)
					});
					$(this).find("a.link-iso3").each(function( i ) {
						var linkHref = $(this).attr("href");
						$(this).attr("href", linkHref.slice(0,-3) + currentCountry.iso3)
					});
					$(this).find("a.link-country-name").each(function( i ) {
						var linkHref = $(this).attr("href");
						linkHref = linkHref.substr(0, linkHref.lastIndexOf("\\"));
						$(this).attr("href", linkHref + currentCountry.name)
					});
					var linkHref = $(this).find('a.link-un-data').attr("href");
					$(this).find('a.link-un-data').attr("href", linkHref.slice(0,-7) + currentCountry.iso2 + '.html');
				});
				/* 
				## End Profile ##
				*/
				/* 
				## Start Happening Now Fires ##
				*/
				$('#collapseFires').on('shown.bs.collapse', function () {
					var cardTwigName = 'Fires'; //Set this once here as so there are no mistakes below.
					$( "div.fires_1d" ).tooltip({
							title: moment().format('MMMM D, YYYY'),
					});
					$( "div.fires_7d" ).tooltip({
							title: moment().subtract(7, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
					});
					$( "div.fires_30d" ).tooltip({
							title: moment().subtract(30, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
					});
					$( "div.fires_90d" ).tooltip({
							title: moment().subtract(90, 'days').format('MMMM D, YYYY') + " to " + moment().format('MMMM D, YYYY'),
					});
					var layerArgs = moment().subtract(1, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
					//$( "button[data-card-id='"+cardTwigName+"'].active" ).removeClass("active");
					//$( "button[data-layer-id='fires_1d']" ).addClass("active"); //forcing fires at 1 day to be active since the args are needed and this is the way
					$().addMapLayer(cardTwigName, layerArgs, "fires_1d"); 
					$( "button.layer-button#layer-button-fires_1d" ).click(function( event ) {
						var layerArgs = moment().subtract(1, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addMapLayer(cardTwigName, layerArgs, "fires_1d"); 
					});
					$( "button.layer-button#layer-button-fires_7d" ).click(function( event ) {
						var layerArgs = moment().subtract(7, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addMapLayer(cardTwigName, layerArgs, "fires_7d"); 
					});
					$( "button.layer-button#layer-button-fires_30d" ).click(function( event ) {
						var layerArgs = moment().subtract(30, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addMapLayer(cardTwigName, layerArgs, "fires_30d"); 
					});
					$( "button.layer-button#layer-button-fires_90d" ).click(function( event ) {
						var layerArgs = moment().subtract(90, 'days').format('YYYY-MM-DD')+'/'+moment().format('YYYY-MM-DD');
						$().addMapLayer(cardTwigName, layerArgs, "fires_90d");  
					});
				});
				/* 
				## End Happening Now Fires ##
				*/
				/* 
				## Start Happening Now Floods ##
				*/
				$('#collapseFloods').on('shown.bs.collapse', function () {
					var cardTwigName = 'Floods'; //Set this once here as so there are no mistakes below.
					var layerArgs = moment().format('YYYY-MM-DD');
					$().addMapLayer(cardTwigName, layerArgs, "floods");
				});
				/* 
				## End Happening Now Floods ##
				*/
				/* 
				## Start Happening Now Droughts ##
				*/
				$('#collapseDroughts').on('shown.bs.collapse', function () {
					var cardTwigName = 'Droughts'; //Set this once here as so there are no mistakes below.
					var edoDay = moment().format('DD');
					var edoMonth = moment().format('MM') - 1;
					if(edoDay <=10) {edoDay = '01'}else if(edoDay >=11 && edoDay <= 20){edoDay = '11'}else if(edoDay >=21 ){edoDay = '21'}else{}
					var layerArgs = 'SELECTED_YEAR=' + moment().format('YYYY') + '&SELECTED_MONTH=' + edoMonth + '&SELECTED_TENDAYS='+edoDay;
					$().addMapLayer(cardTwigName, layerArgs); 
				});
				$('#collapseDroughts').on('hide.bs.collapse', function () {
					var cardTwigName = 'Droughts'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Happening Now Droughts ##
				*/
				/* 
				## Start Happening Now Sea Surface Temperature Anomalies ##
				*/
				$('#collapseSST').on('shown.bs.collapse', function () {
					var cardTwigName = 'SST'; //Set this once here as so there are no mistakes below.
					 
				});
				$('#collapseSST').on('hide.bs.collapse', function () {
					var cardTwigName = 'SST'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Happening Now Sea Surface Temperature Anomalies ##
				*/
				/* 
				## Start Happening Now Coral Bleaching Heat Stress Alerts ##
				*/
				$('#collapseCoralBleaching').on('shown.bs.collapse', function () {
					var cardTwigName = 'CoralBleaching'; //Set this once here as so there are no mistakes below.
					  
				});
				$('#collapseCoralBleaching').on('hide.bs.collapse', function () {
					var cardTwigName = 'CoralBleaching'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Happening Now Coral Bleaching Heat Stress Alerts ##
				*/
				/* 
				## Start Climate Elevation Profile  ##
				*/
				$('#collapseElevationProfile').on('shown.bs.collapse', function () {
					var cardTwigName = 'ElevationProfile'; //Set this once here as so there are no mistakes below.
					 
					var indicatorTitle = "Virtual elevation profile for " + currentCountry.name; 
					var tableChartColors = ['#65a70c'];
					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle,
						yAxisTitle: "Elevation (m) above sea level",
						xAxis: {
							type: 'category',
							data: [ 'Min.' , 'Max.' ]
						},
						series: [{
						  name: 'Elevation',
						  data: [allCountryInds.gebco_min_m, allCountryInds.gebco_max_m],
						  type: 'line',
						  areaStyle: {
							color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
								offset: 0,
								color: '#65a70c'
							}, {
								offset: 1,
								color: '#ffe'
							}])
						  },
						  label: {
							  show: true,
							  formatter: function(d) { 
								return d.name + ' ' + d.data + 'm';
							  }
						  },
						  markLine: {
							  data: [{
								type: "average",
								name: 'Average',
							  }],
							  label: {
								show: true,
								position: "insideEndTop",
								formatter: '{b}: {c}m',
							  }
						  },
						}]
					}
					$().createXYAxisChart(cardTwigName , chartData);  
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.gebco_min_m, allCountryInds.gebco_mean_m, allCountryInds.gebco_max_m, allCountryInds.gebco_stdev_m]]; 
						var columnData = [
							{ title: "Min. (m)" },
            				{ title: "Mean (m)" },
            				{ title: "Max. (m)" },
            				{ title: "Std. Dev. (m)" },
						];
						var columnSettings = [{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": "_all" }];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				/* 
				## End Climate Elevation Profile  ##
				*/
				 
				/* 
				## Start Ecosystems Terrestrial ecoregions and Marine ecoregions  ##
				*/
				$('#collapseTerrestrialEcoregions, #collapseMarineEcoregions').on('shown.bs.collapse', function () {
					//$().checkForDataTables("TerrestrialEcoregions"); //marine and terrestrial are linked, if one is there we can remove both, so just need to check one. Same for creating below
					//Datatable Complex
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$(currentTableDiv).append(miniLoadingSpinner);
					//https://dopa-services.jrc.ec.europa.eu/services/d6dopa40/administrative_units/get_country_ecoregions_stats?format=json&c_un_m49=516
					var countryEcroregionUrl = DopaBaseUrl+'administrative_units/get_country_ecoregions_stats?format=json&b_iso2='+currentCountry.iso2;
					$.getJSON(countryEcroregionUrl,function(d){
						var terrestrialDataSet = [];
						var marineDataSet = [];
						$(d.records).each(function(i, data) {
							var newDataRow = [];
							newDataRow.push(data.ecoregion, data.area_km2, data.percentage_of_ecoregion_in_country, data.percentage_of_ecoregion_protected_in_country, data.country_contribution_to_global_ecoregion_protection, data.ecoregion_protection_percentage);
							if(data.is_marine){
								marineDataSet.push(newDataRow); 
							} else {
								terrestrialDataSet.push(newDataRow); 
							}
						});
						var columnData = [
							{title: "Name"},
							{title: "Area (km\u00B2)"},
							{title: "% of ecoregion in country"},
							{title: "% of ecoregion protected in country"},
							{title: "% country contribution to global protection"},
							{title: "% of ecoregion protected worldwide"},
						];
						var columnSettings = [{ 
							"targets": [ 2, 3, 4 ],
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								$(cell).html(cellData+'<div class="table-indicator-bar aichinoc'+(colIndex-1)+'"></div>');
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						},{ 
							"targets": [ 5 ],
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								if (cellData >= 17){
								  $(cell).html(cellData+'<div class="table-aichi-bar aichiyes rounded-left">Aichi target 11 met</div>');
								} else if (cellData  >= 10 && cellData <  17) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino1">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 5 && cellData <  10) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino2">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 2 && cellData <  5) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino3">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 1 && cellData <  2) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino4">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino5">'+(17-cellData).toFixed(2)+'% missing</div>'); 
								}
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						}];
						var terrestrialTableData = {
							title: "Terrestrial Ecoregions for " + currentCountry.name,
							columns: columnData,
							columnDefs: columnSettings,
							data: terrestrialDataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						var marineTableData = {
							title: "Terrestrial Ecoregions for " + currentCountry.name,
							columns: columnData,
							columnDefs: columnSettings,
							data: terrestrialDataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable('TerrestrialEcoregions', terrestrialTableData);
						$().createDataTable('MarineEcoregions', marineTableData);
					});
				});
				/* 
				## End Ecosystems Terrestrial ecoregions and Marine ecoregions  ##
				*/
				 
				/* 
				## Start Ecosystems Inland Surface Water  ##
				*/
				$('#collapseInlandSurfaceWater').on('shown.bs.collapse', function () {
					var cardTwigName = 'InlandSurfaceWater'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Area of Permanent and Seasonal Water for " + currentCountry.name; 
					var tableChartColors = ['#1710f7','#c184c1']; //this will be used to coordinate the coloring of BOTH table and chart
					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle,
						yAxisTitle: "Area (km\u00B2)",
						xAxis: {
							type: 'category',
							data: ['1984','2018']
						},
						series: [{
						  name: 'Permanent Water',
						  data: [ parseFloat(allCountryInds.water_p_now_km2)-(parseFloat(allCountryInds.water_p_netchange_km2)) , allCountryInds.water_p_now_km2],
						  type: 'bar',
						},{
						  name: 'Seasonal  Water',
						  data: [ parseFloat(allCountryInds.water_s_now_km2)-(parseFloat(allCountryInds.water_s_netchange_km2)) , allCountryInds.water_s_now_km2],
						  type: 'bar',
						}]
					}
					
					$().createXYAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.water_p_now_km2, allCountryInds.water_s_now_km2, allCountryInds.water_p_netchange_km2, allCountryInds.water_s_netchange_km2, allCountryInds.water_p_netchange_perc, allCountryInds.water_s_netchange_perc]]; 
						var columnData = [
							{ title: "Area (km\u00B2) of permanent surface water (2018)" },
            				{ title: "Area (km\u00B2) of seasonal inland water (2018)" },
            				{ title: "Net change (km\u00B2) of permanent surface water (2018 – 1984)" },
            				{ title: "Net change (km\u00B2) of seasonal inland water " },
							{ title: "Net change (%) of permanent surface water (2018 – 1984)" },
							{ title: "Net change (%) in surface area of seasonal inland water " },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 5 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapseInlandSurfaceWater').on('hide.bs.collapse', function () {
					var cardTwigName = 'InlandSurfaceWater'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Ecosystems Inland Surface Water  ##
				*/
				
				/* 
				## Start Ecosystems Forest cover  ##
				*/
				$('#collapseForestCover').on('shown.bs.collapse', function () {
					var cardTwigName = 'ForestCover'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Forest loss and gain in ecoregions for " + currentCountry.name; 
					var tableChartColors = ['#ff0000','#0000ff'];
					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle,
						yAxisTitle: "Percent",
						xAxis: {
							type: 'category',
							data: ['Forest Loss','Forest Gain']
						},
						series: [{
						  data: [{
      						value: allCountryInds.gfc_loss_perc,
							itemStyle: {
        						color: "#ff0000"
      						}
    					  },{
      						value: allCountryInds.gfc_gain_perc,
							itemStyle: {
        						color: "#0000ff"
      						}
    					  }],
						  type: 'bar',
						}] 
					}
					
					$().createXYAxisChart(cardTwigName , chartData); 
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.gfc_treecover_km2, allCountryInds.gfc_treecover_perc, allCountryInds.gfc_loss_km2, allCountryInds.gfc_loss_perc, allCountryInds.gfc_gain_km2, allCountryInds.gfc_gain_perc]]; 
						var columnData = [
							{ title: "Forest cover (km\u00B2)" },
            				{ title: "Forest cover (%)" },
            				{ title: "Forest loss (km\u00B2)" },
            				{ title: "Forest loss (%)" },
							{ title: "Forest gain (km\u00B2)" },
							{ title: "Forest gain (%)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 5 ] },
							{ "className": "dt-center", "targets": "_all"}
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapseForestCover').on('hide.bs.collapse', function () {
					var cardTwigName = 'ForestCover'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Ecosystems Forest cover  ##
				*/
				
				/* 
				## Start Ecosystems Land Degradation  ##
				*/
				$('#collapseLandDegradation').on('shown.bs.collapse', function () {
					var cardTwigName = 'LandDegradation'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Land degradation for " + currentCountry.name; 
					var tableChartColors = ['#ab2828', '#ed7325', '#ffd954', '#b8d879', '#46a246','#c2c5cc'];
					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle, 
						series: [{
							name: indicatorTitle,
							type: 'pie',
							radius: ['50%', '70%'],
							avoidLabelOverlap: true,
							data: [
								{value: allCountryInds.lpd_severe_km2, name: 'Persistent severe decline in productivity'},
								{value: allCountryInds.lpd_moderate_km2, name: 'Persistent moderate decline in productivity'},
								{value: allCountryInds.lpd_stressed_km2, name: 'Stable, but stressed'},
								{value: allCountryInds.lpd_stable_km2, name: 'Stable Productivity'},
								{value: allCountryInds.lpd_increased_km2, name: 'Persistent increase in productivity'},
								{value: allCountryInds.lpd_null_km2, name: 'No biomass'},
							] 
						}]
					}
					
					$().createNoAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.lpd_null_km2, allCountryInds.lpd_severe_km2, allCountryInds.lpd_moderate_km2, allCountryInds.lpd_stressed_km2, allCountryInds.lpd_stable_km2, allCountryInds.lpd_increased_km2]]; 
						var columnData = [
							{ title: "No biomass (km\u00B2)" },
            				{ title: "Persistent severe decline in productivity (km\u00B2)" },
            				{ title: "Persistent moderate decline in productivity (km\u00B2)" },
            				{ title: "Stable, but stressed; persistent strong inter-annual productivity variations (km\u00B2)" },
							{ title: "Stable Productivity (km\u00B2)" },
							{ title: "Persistent increase in productivity (km\u00B2)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-5", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-3", "targets": [ 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-4", "targets": [ 5 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapseLandDegradation').on('hide.bs.collapse', function () {
					var cardTwigName = 'LandDegradation'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Ecosystems Land Degradation  ##
				*/
				
				/* 
				## Start Ecosystems Land Fragmentation  ##
				*/
				$('#collapseLandFragmentation').on('shown.bs.collapse', function () {
					var cardTwigName = 'LandFragmentation'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Natural land pattern fragmentation for " + currentCountry.name + " (1995-2015)";
					var tableChartColors = ['#7fad41', '#878787', '#52781f', '#f1c200', '#e89717','#d56317'];
					
					//an object is used here to calculate the percentages.
					var landFragmentationData = {
						core: [allCountryInds.mspa_core_1995_km2, allCountryInds.mspa_core_2000_km2, allCountryInds.mspa_core_2005_km2, allCountryInds.mspa_core_2010_km2, allCountryInds.mspa_core_2015_km2],
						non_natural: [allCountryInds.mspa_non_natural_1995_km2, allCountryInds.mspa_non_natural_2000_km2, allCountryInds.mspa_non_natural_2005_km2, allCountryInds.mspa_non_natural_2010_km2, allCountryInds.mspa_non_natural_2015_km2],
						edges: [allCountryInds.mspa_edges_1995_km2, allCountryInds.mspa_edges_2000_km2, allCountryInds.mspa_edges_2005_km2, allCountryInds.mspa_edges_2010_km2, allCountryInds.mspa_edges_2015_km2],
						perforation: [allCountryInds.mspa_perforation_1995_km2, allCountryInds.mspa_perforation_2000_km2, allCountryInds.mspa_perforation_2005_km2, allCountryInds.Perforation_2010, allCountryInds.mspa_perforation_2015_km2],
						islets: [allCountryInds.mspa_islets_1995_km2, allCountryInds.mspa_islets_2000_km2, allCountryInds.mspa_islets_2005_km2, allCountryInds.mspa_islets_2010_km2, allCountryInds.mspa_islets_2015_km2],
						margins: [allCountryInds.mspa_margins_1995_km2, allCountryInds.mspa_margins_2000_km2, allCountryInds.mspa_margins_2005_km2, allCountryInds.mspa_margins_2010_km2, allCountryInds.mspa_margins_2015_km2],
					};
					/* $.each( landFragmentationData, function( key, value ) {
						var sumData = 0;
						$.each(value,function(){sumData+=parseFloat(this) || 0;});
						console.log(sumData)
						value.forEach(function(num, index){
						   this[index] = (100 * num / sumData).toFixed(2);
						}, value);
					}); */

					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle,
						yAxisTitle: "km\u00B2",
						xAxis: {
							type: 'category',
							data: ['1995','2000','2005','2010','2015']
						},
						series: [{
							name: 'Core',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.core,
						}, {
							name: 'Non Natural',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.non_natural,
						}, {
							name: 'Edge',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.edges,
						}, {
							name: 'Core Perforation',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.perforation,
						},{
							name: 'Islet',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.islets,
						}, {
							name: 'Linear',
							type: 'line',
							stack: '1',
							areaStyle: {},
							data: landFragmentationData.margins,
						}]
					}
					
					$().createXYAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [
							[1995, allCountryInds.mspa_core_1995_km2, allCountryInds.mspa_non_natural_1995_km2, allCountryInds.mspa_edges_1995_km2, allCountryInds.mspa_perforation_1995_km2, allCountryInds.mspa_islets_1995_km2, allCountryInds.mspa_margins_1995_km2],
							[2000, allCountryInds.mspa_core_2000_km2, allCountryInds.mspa_non_natural_2000_km2, allCountryInds.mspa_edges_2000_km2, allCountryInds.mspa_perforation_2000_km2, allCountryInds.mspa_islets_2000_km2, allCountryInds.mspa_margins_2000_km2],
							[2005, allCountryInds.mspa_core_2005_km2, allCountryInds.mspa_non_natural_2005_km2, allCountryInds.mspa_edges_2005_km2, allCountryInds.mspa_perforation_2005_km2, allCountryInds.mspa_islets_2005_km2, allCountryInds.mspa_margins_2005_km2],
							[2010, allCountryInds.mspa_core_2010_km2, allCountryInds.mspa_non_natural_2010_km2, allCountryInds.mspa_edges_2010_km2, allCountryInds.mspa_perforation_2010_km2, allCountryInds.mspa_islets_2010_km2, allCountryInds.mspa_margins_2010_km2],
							[2015, allCountryInds.mspa_core_2015_km2, allCountryInds.mspa_non_natural_2015_km2, allCountryInds.mspa_edges_2015_km2, allCountryInds.mspa_perforation_2015_km2, allCountryInds.mspa_islets_2015_km2, allCountryInds.mspa_margins_2015_km2]];  
						var columnData = [
							{ title: "Year" },
							{ title: "Core (km\u00B2)" },
            				{ title: "Non Natural (km\u00B2)" },
            				{ title: "Edge (km\u00B2)" },
            				{ title: "Core Perforation (km\u00B2)" },
							{ title: "Islet (km\u00B2)" },
							{ title: "Linear (km\u00B2)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-3", "targets": [ 4 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-4", "targets": [ 5 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-5", "targets": [ 6 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapseLandFragmentation').on('hide.bs.collapse', function () {
					var cardTwigName = 'LandFragmentation'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Ecosystems Land Fragmentation  ##
				*/
				
				/* 
				## Start Ecosystem Services Below Ground Carbon  ##
				*/
				$('#collapseBelowGroundCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'BelowGroundCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Below ground carbon for " + currentCountry.name; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.bgb_min_c_mg_total, allCountryInds.bgb_mean_c_mg_total, allCountryInds.bgb_max_c_mg_total, allCountryInds.bgb_stdev_c_mg_total, allCountryInds.bgb_tot_c_pg_total]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
            				{ title: "Std. Dev. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 4 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapseBelowGroundCarbon').on('hide.bs.collapse', function () {
					var cardTwigName = 'BelowGroundCarbon'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Ecosystem Services Below Ground Carbon  ##
				*/
				
				/* 
				## Start Ecosystem Services Soil Organic Carbon  ##
				*/
				$('#collapseSoilOrganicCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'SoilOrganicCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Soil organic carbon for " + currentCountry.name; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.gsoc_min_c_mg_total, allCountryInds.gsoc_mean_c_mg_total, allCountryInds.gsoc_max_c_mg_total, allCountryInds.gsoc_stdev_c_mg_total, allCountryInds.gsoc_tot_c_pg_total]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
            				{ title: "Std. Dev. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 4 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapseSoilOrganicCarbon').on('hide.bs.collapse', function () {
					var cardTwigName = 'SoilOrganicCarbon'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Ecosystem Services Soil Organic Carbon  ##
				*/
				/* 
				## Start Ecosystem Services Above ground carbon  ##
				*/
				$('#collapseAboveGroundCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'AboveGroundCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Above ground carbon for " + currentCountry.name; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.agb_min_c_mg_total, allCountryInds.agb_mean_c_mg_total, allCountryInds.agb_max_c_mg_total, allCountryInds.agb_stdev_c_mg_total, allCountryInds.agb_tot_c_pg_total]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
            				{ title: "Std. Dev. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 4 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapseAboveGroundCarbon').on('hide.bs.collapse', function () {
					var cardTwigName = 'AboveGroundCarbon'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Ecosystem Services Above ground carbon  ##
				*/
				/* 
				## Start Ecosystem Services Total carbon  ##
				*/
				$('#collapseTotalCarbon').on('shown.bs.collapse', function () {
					var cardTwigName = 'TotalCarbon'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Total carbon for " + currentCountry.name; 
					var tableChartColors = ['#d7191c', '#1a9641'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.carbon_min_c_mg_total, allCountryInds.carbon_mean_c_mg_total, allCountryInds.carbon_max_c_mg_total, allCountryInds.carbon_stdev_c_mg_total, allCountryInds.carbon_tot_c_pg_total]]; 
						var columnData = [
							{ title: "Min. (Mg)" },
            				{ title: "Mean (Mg)" },
            				{ title: "Max. (Mg)" },
            				{ title: "Std. Dev. (Mg)" },
							{ title: "Sum (Pg)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 0 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 4 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapseTotalCarbon').on('hide.bs.collapse', function () {
					var cardTwigName = 'TotalCarbon'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Ecosystem Services Total carbon  ##
				*/
				/* 
				## Start Land Cover Copernicus Global Land Cover 2015  ##
				*/
				$('#collapseCopernicusGLC').on('shown.bs.collapse', function () {
					var cardTwigName = 'CopernicusGLC'; //Set this once here as so there are no mistakes below.
					
					var indicatorTitle = "Copernicus Land Cover " + currentCountry.name; 
					var copernicusLandCoverUrl = DopaBaseUrl+'landcover/get_country_lc_copernicus?year=2015&agg=0&iso=un_m49&country='+currentCountry.un;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(copernicusLandCoverUrl,function(d){
							var dataSet = [];
							var chartSeriesData = [];
							var tableChartColors = [];
							//var dataSet = [[allCountryInds.carbon_min_c_mg_total, allCountryInds.carbon_mean_c_mg_total, allCountryInds.carbon_max_c_mg_total, allCountryInds.carbon_stdev_c_mg_total, allCountryInds.carbon_tot_c_pg_total]];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								newDataRow.push(data.label, data.percent, data.area, data.color);
								dataSet.push(newDataRow); 
								chartSeriesData.push({'name': data.label,'value': data.area}) 
								tableChartColors.push(data.color) 
							});
							var chartData = {
								colors: tableChartColors,
								title: indicatorTitle, 
								series: [{
									type: 'treemap',
									data: chartSeriesData
								}]
							}
							$().createNoAxisChart(cardTwigName , chartData); 

							var columnData = [
								{ title: "Land Cover Class" },
								{ title: "% Covered" },
								{ title: "Calculated Surface (km2)" },
								{ title: "Color Map" },
							];
							var columnSettings = [
								{
									"targets": 3,
									"createdCell": function (td, cellData, rowData, row, col) {
										$(td).css('color', cellData);
										$(td).css('background', cellData);
									},
								},
								{ "className": cardTwigName, "targets": [ "_all" ] }
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true
							}
							$().createDataTable(cardTwigName, tableData); 
							$().updateCellColors(cardTwigName, tableChartColors);
						});
					}
				});
				$('#collapseCopernicusGLC').on('hide.bs.collapse', function () {
					var cardTwigName = 'CopernicusGLC'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Land Cover Copernicus Global Land Cover 2015  ##
				*/
				/* 
				## Start Land Cover ESA Land Cover change 1995-2015  ##
				*/
				$('#collapseEsaLC').on('shown.bs.collapse', function () {
					var cardTwigName = 'EsaLC'; //Set this once here as so there are no mistakes below.
					
				});
				$('#collapseEsaLC').on('hide.bs.collapse', function () {
					var cardTwigName = 'EsaLC'; //Set this once here as so there are no mistakes below.
					
				});
				/* 
				## End Land Cover ESA Land Cover change 1995-2015  ##
				*/
				/* 
				## Start Species Species Richness  ##
				*/
				$('#collapseSpeciesLayers').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesLayers'; //Set this once here as so there are no mistakes below.

				});
				$('#collapseSpeciesLayers').on('hide.bs.collapse', function () {
					var cardTwigName = 'SpeciesLayers'; //Set this once here as so there are no mistakes below.

				});
				/* 
				## End Species Species Richness  ##
				*/
				/* 
				## Start Species Species Reported number of animal and plant species ##
				*/
				$('#collapseSpeciesAnimalPlantNumbers').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesAnimalPlantNumbers'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Reported number of animal and plant species in " + currentCountry.name; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					//
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
						$(currentTableDiv).append(miniLoadingSpinner);
						var speciesAnimalPlantNumbersUrl = DopaBaseUrl+'species/get_country_species_total?format=json&b_iso2='+currentCountry.iso2;
						$.getJSON(speciesAnimalPlantNumbersUrl,function(d){
							var data = d.records[0];
							var dataSet = [['Number', data.animals, data.plants, data.total], ['Country Ranking', data.animals_rank, data.plants_rank, data.total_rank]];

							var columnData = [
								{title: ""},
								{title: "Animal species assessed by IUCN"},
								{title: "Plant species assessed by IUCN"},
								{title: "Total species assessed by IUCN"},
							];
							var columnSettings = [{ "className": "dt-center", "targets": [ 1,2,3 ] }];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable(cardTwigName, tableData);
						});
					}
				});
				$('#collapseSpeciesAnimalPlantNumbers').on('hide.bs.collapse', function () {
					var cardTwigName = 'SpeciesAnimalPlantNumbers'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Species Species Reported number of animal and plant species ##
				*/
				/* 
				## Start Species Reported number of threatened amphibians, birds and mammals ##
				*/
				$('#collapseSpeciesThreatNumbers').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesThreatNumbers'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Reported number of threatened amphibians, birds and mammals " + currentCountry.name; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					//
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
						$(currentTableDiv).append(miniLoadingSpinner);
						var speciesAnimalPlantNumbersUrl = DopaBaseUrl+'species/get_country_threatened_animals?format=json&b_iso2='+currentCountry.iso2;
						$.getJSON(speciesAnimalPlantNumbersUrl,function(d){
							var data = d.records[0];
							var dataSet = [['Number', data.amphibians, data.birds, data.mammals, data.total], ['Country Ranking', data.amphibians_rank, data.birds_rank, data.mammals_rank, data.total_rank]];

							var columnData = [
								{title: ""},
								{title: "Threatened Amphibians"},
								{title: "Threatened Birds"},
								{title: "Threatened Mammals"},
								{title: "Total"},
							];
							var columnSettings = [{ "className": "dt-center", "targets": [ 1,2,3,4 ] }];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable(cardTwigName, tableData);
						});
					}
				});
				$('#collapseSpeciesThreatNumbers').on('hide.bs.collapse', function () {
					var cardTwigName = 'SpeciesThreatNumbers'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Species Reported number of threatened amphibians, birds and mammals ##
				*/
				/* 
				## Start Species Reported endemic and threatened endemic vertebrates in the country ##
				*/
				$('#collapseSpeciesThreatEndemicNumbers').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesThreatEndemicNumbers'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Reported endemic and threatened endemic vertebrates in " + currentCountry.name; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					//
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
						$(currentTableDiv).append(miniLoadingSpinner);
						var speciesAnimalPlantNumbersUrl = DopaBaseUrl+'species/get_country_endemics_threatened_vertebrates?format=json&b_iso2='+currentCountry.iso2;
						$.getJSON(speciesAnimalPlantNumbersUrl,function(d){
							var data = d.records[0]; 
							var dataSet = [['Country Endemic', data.amphibians_endemics, data.birds_endemics, data.mammals_endemics, data.sharks_rays_endemics, data.total_endemics], ['Threatened Country Endemic', data.amphibians_threatened_endemics, data.birds_threatened_endemics, data.mammals_threatened_endemics, data.sharks_rays_threatened_endemics, data.total_threatened_endemics]];

							var columnData = [
								{title: ""},
								{title: "Amphibians"},
								{title: "Birds"},
								{title: "Mammals"},
								{title: "Sharks and Rays"},
								{title: "Total"},
							];
							var columnSettings = [{ "className": "dt-center", "targets": [ 1,2,3,4,5 ] }];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable(cardTwigName, tableData);
						});
					}
				});
				$('#collapseSpeciesThreatEndemicNumbers').on('hide.bs.collapse', function () {
					var cardTwigName = 'SpeciesThreatEndemicNumbers'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Species Reported endemic and threatened endemic vertebrates in the country ##
				*/
				/* 
				## Start Species Threatened and near threatened species (computed)  ##
				*/
				$('#collapseSpeciesComputed').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesComputed'; //Set this once here as so there are no mistakes below.
					//
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$(currentTableDiv).append(miniLoadingSpinner);
					var indicatorTitle = "Threatened and near threatened species (computed) for " + currentCountry.name; 
					var speciesRedListUrl = DopaBaseUrl+'species/get_country_redlist_th_list?format=json&c_un_m49=' + currentCountry.un;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(speciesRedListUrl,function(d){
							var dataSet = [];
							var chartSeriesData = [];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								newDataRow.push('<a href="https://www.iucnredlist.org/search?query='+data.id_no+'&searchType=species"target="_blank"><img class="iucn-redlist-img" src="'+imagePath+'redlist50sm.png" alt="RED LIS IUCN"></a>', data.binomial, data.kingdom, data.phylum,data.order, data.family, data.code);
								dataSet.push(newDataRow); 
							});
							var columnData = [
								{ title: "IUCN Red List ID" },
								{ title: "Scientific Name" },
								{ title: "Kingdom" },
								{ title: "Phylum" },
								{ title: "Order" },
								{ title: "Family" },
								{ title: "Status" },
							];
							var columnSettings = [
								{
									"targets": 6,
									"className": "dt-center",
									"createdCell": function (td, cellData, rowData, row, col) { 
										switch(cellData) {
										  	case "EX":
												$(td).html('<div class="species-table-status species-ex">Extinct (EX)</div>');
												break;
										  	case "EW":
												$(td).html('<div class="species-table-status species-ew">Extinct in the Wild (EW)</div>');
												break;
											case "CR":
												$(td).html('<div class="species-table-status species-cr">Critically Endangered (CR)</div>');
												break;
											case "EN":
												$(td).html('<div class="species-table-status species-en">Endangered (EN)</div>');
												break;
											case "VU":
												$(td).html('<div class="species-table-status species-vu">Vulnerable (VU)</div>');
												break;
											case "NT":
												$(td).html('<div class="species-table-status species-nt">Near Threatened (NT)</div>');
												break;
											case "LC":
												$(td).html('<div class="species-table-status species-lc">Least Concern (LC)</div>');
												break;
										  	default:
												$(td).html('<div class="species-table-status species-dd">Data Deficient (DD)</div>');
										}
									},
								},
								{ "className": cardTwigName, "targets": [ "_all" ] }
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true
							}
							$().createDataTable(cardTwigName, tableData); 
						});
					}
				});
				$('#collapseSpeciesComputed').on('hide.bs.collapse', function () {
					var cardTwigName = 'SpeciesNumbers'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Species Threatened and near threatened species (computed)  ##
				*/
				/* 
				## Start Species occurrences reported to the GBIF ##
				*/
				$('#collapseSpeciesGbif').on('shown.bs.collapse', function () {
					var cardTwigName = 'SpeciesGbif'; //Set this once here as so there are no mistakes below.
					//
				});
				$('#collapseSpeciesGbif').on('hide.bs.collapse', function () {
					var cardTwigName = 'SpeciesGbif'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Species occurrences reported to the GBIF ##
				*/
				/* 
				## Start Conservation Protected area coverage and connectivity  ##
				*/
				$('#collapsePaProtCon').on('shown.bs.collapse', function () {
					var cardTwigName = 'PaProtCon'; //Set this once here as so there are no mistakes below.
					//
					var indicatorTitle = "Protected area coverage and connectivity for " + currentCountry.name; 
					var tableChartColors = ['rgb(202,229,161)','rgb(142,207,224)','rgb(127,173,65)'];
					//Chart
					var chartData = {
						colors: tableChartColors,
						title: indicatorTitle,
						yAxisTitle: "Percent",
						legend: {}, 
						xAxis: {
							type: 'category',
							data: ['Protected Land Area', 'Protected Marine Area', 'Protected Connected Land']
						},
						series: [{
						  data: [{
      						value: allCountryInds.area_prot_terr_perc,
							itemStyle: {
        						color: "rgb(202,229,161)"
      						}
    					  },{
      						value: allCountryInds.area_prot_mar_perc,
							itemStyle: {
        						color: "rgb(142,207,224)"
      						}
    					  },{
      						value: allCountryInds.protconn,
							itemStyle: {
        						color: "rgb(127,173,65)"
      						}
    					  }],
						  type: 'bar',
						}]
					}
					
					$().createXYAxisChart(cardTwigName , chartData); 
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.area_terr_km2, allCountryInds.area_prot_terr_km2, allCountryInds.area_prot_terr_perc, allCountryInds.area_mar_km2, allCountryInds.area_prot_mar_km2, allCountryInds.area_prot_mar_perc, allCountryInds.protconn]]; 
						var columnData = [
							{ title: "Total Land Area" },
            				{ title: "Protected Land Area" },
            				{ title: "Terrestrial Coverage" },
            				{ title: "Total Marine Area" },
							{ title: "Protected Marine Area" },
							{ title: "Marine Coverage" },
							{ title: "Protected Connected Land" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 5 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 6 ] },
							{ "className": "dt-center", "targets": "_all"}
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapsePaProtCon').on('hide.bs.collapse', function () {
					var cardTwigName = 'PaProtCon'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Conservation Protected area coverage and connectivity  ##
				*/
				/* 
				## Start Conservation Country protection relative to Aichi Target 11  ##
				*/
				$('#collapseCountryAichiProt').on('shown.bs.collapse', function () {
					var cardTwigName = 'CountryAichiProt'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Protection relative to Aichi Target 11 for " + currentCountry.name; 
					var tableChartColors = ['#1f814a','#1078b4'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.area_prot_terr_perc, allCountryInds.area_prot_mar_perc]]; 
						var columnData = [
							{ title: "Progress towards Aichi Target 11: 17% terrestrial coverage" },
            				{ title: "Progress towards Aichi Target 11: 10% marine coverage" },
						];
						var columnSettings = [{
							"targets": [ 0 ],
							"className": "dt-center "+cardTwigName+"-cell-color-0",
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								if (cellData >= 17){
								  $(cell).html(cellData+'<div class="table-aichi-bar aichiyes rounded-left">Aichi target 11 met</div>');
								} else if (cellData  >= 10 && cellData <  17) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino1">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 5 && cellData <  10) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino2">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 2 && cellData <  5) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino3">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 1 && cellData <  2) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino4">'+(17-cellData).toFixed(2)+'% missing</div>');
								} else {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino5">'+(17-cellData).toFixed(2)+'% missing</div>'); 
								}
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						},{
							"targets": [ 1 ],
							"className": "dt-center "+cardTwigName+"-cell-color-1",
							"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
								if (cellData >= 10){
								  $(cell).html(cellData+'<div class="table-aichi-bar aichiyes rounded-left">Aichi target 11 met</div>');
								} else if (cellData  >= 8 && cellData <  10) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino1">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 4 && cellData <  8) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino2">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 2 && cellData <  4) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino3">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else if (cellData  >= 1 && cellData <  1) {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino4">'+(10-cellData).toFixed(2)+'% missing</div>');
								} else {
								  $(cell).html(cellData+'<div class="table-aichi-bar aichino5">'+(10-cellData).toFixed(2)+'% missing</div>'); 
								}
								$(cell).find('.aichinoc'+(colIndex-1)).css('width', parseInt(cellData));
							}
						}];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
				});
				$('#collapseCountryAichiProt').on('hide.bs.collapse', function () {
					var cardTwigName = 'CountryAichiProt'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Conservation Country protection relative to Aichi Target 11  ##
				*/
				/* 
				## Start Conservation Number of protected areas  ##
				*/
				$('#collapseNumPas').on('shown.bs.collapse', function () {
					var cardTwigName = 'NumPas'; //Set this once here as so there are no mistakes below.
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					var tableChartColors = ['#7fad41','#8fcde5','#e29864'];
					$(currentTableDiv).append(miniLoadingSpinner);
					var indicatorTitle = "Number of Terrestrial, Marine and Coastal protected areas for " + currentCountry.name; 
					var paNumbersUrl = DopaBaseUrl+'administrative_units/get_country_pa_count?format=json&b_iso2=' + currentCountry.iso2;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(paNumbersUrl,function(d){
							var dataSet = [];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								newDataRow.push(data.pa_count, data.pa_count_polygons, data.pa_count_points, data.pa_count_polygons_terrestrial, data.pa_count_points_terrestrial, data.pa_count_polygons_marine, data.pa_count_points_marine, data.pa_count_polygons_coastal, data.pa_count_points_coastal);
								dataSet.push(newDataRow); 
							});
							
							var columnData = [
								{ title: "Total number of all PAs (all geometries)" },
								{ title: "# all polygon PAs" },
								{ title: "# all point PAs" },
								{ title: "# <b>terrestrial</b> PAs (polygons)" },
								{ title: "# <b>terrestrial</b> PAs (points)" },
								{ title: "# <b>marine</b> PAs (polygons)" },
								{ title: "# <b>marine</b> PAs (points)" },
								{ title: "# <b>coastal</b> PAs (polygons)" },
								{ title: "# <b>coastal</b> PAs (points)" },
							];
							var columnSettings = [
								{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 3 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 4 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 5] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 6 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 7 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 8] },
								{ "className": "dt-center", "targets": "_all"}
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable(cardTwigName, tableData); 
							$().updateCellColors(cardTwigName, tableChartColors);
						});
					}
				});
				$('#collapseNumPas').on('hide.bs.collapse', function () {
					var cardTwigName = 'NumPas'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Conservation Number of protected areas  ##
				*/
				/* 
				## Start Conservation Number of protected areas ≥ 10 km2  ##
				*/
				$('#collapseNumPas10').on('shown.bs.collapse', function () {
					var cardTwigName = 'NumPas10'; //Set this once here as so there are no mistakes below.
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					var tableChartColors = ['#7fad41','#8fcde5','#e29864'];
					$(currentTableDiv).append(miniLoadingSpinner);
					var indicatorTitle = "Number of Terrestrial, Marine and Coastal protected areas ≥ 10 km2 for " + currentCountry.name; 
					var paNumbersUrl = DopaBaseUrl+'administrative_units/get_country_pa_count?format=json&b_iso2=' + currentCountry.iso2;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(paNumbersUrl,function(d){
							var dataSet = [];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								newDataRow.push(data.pa_count_o10, data.pa_count_polygons_o10, data.pa_count_points_o10, data.pa_count_polygons_terrestrial_o10, data.pa_count_points_terrestrial_o10, data.pa_count_polygons_marine_o10, data.pa_count_points_marine_o10, data.pa_count_polygons_coastal_o10, data.pa_count_points_coastal_o10);
								dataSet.push(newDataRow); 
							});
							var columnData = [
								{ title: "Total number of all PAs (all geometries)" },
								{ title: "# all polygon PAs" },
								{ title: "# all point PAs" },
								{ title: "# <b>terrestrial</b> PAs (polygons)" },
								{ title: "# <b>terrestrial</b> PAs (points)" },
								{ title: "# <b>marine</b> PAs (polygons)" },
								{ title: "# <b>marine</b> PAs (points)" },
								{ title: "# <b>coastal</b> PAs (polygons)" },
								{ title: "# <b>coastal</b> PAs (points)" },
							];
							var columnSettings = [
								{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 3 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 4 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 5] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 6 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 7 ] },
								{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 8] },
								{ "className": "dt-center", "targets": "_all"}
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: false
							}
							$().createDataTable(cardTwigName, tableData); 
							$().updateCellColors(cardTwigName, tableChartColors);
						});
					}
				});
				$('#collapseNumPas10').on('hide.bs.collapse', function () {
					var cardTwigName = 'NumPas10'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Conservation Number of protected areas ≥ 10 km2  ##
				*/
				/* 
				## Start Conservation List of protected areas ≥ 10 km2  ##
				*/
				$('#collapseListPas10').on('shown.bs.collapse', function () {
					var cardTwigName = 'ListPas10'; //Set this once here as so there are no mistakes below.
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$(currentTableDiv).append(miniLoadingSpinner);
					var indicatorTitle = "List of protected areas ≥ 10 km2 for " + currentCountry.name; 
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [];
						$(allCountryPaStats).each(function(i, data) {
							var newDataRow = [];
							newDataRow.push(data.wdpaid, data.name, data.gis_area, data.nature);
							dataSet.push(newDataRow);
						});
						var columnData = [
							{ title: "WDPA ID" },
							{ title: "Name" },
							{ title: "Area (km\u00B2)" },
							{ title: "Type" },
						];
						var columnSettings = [
							{ "visible": false, "targets": [ 0 ] },
							{
								"targets": [ 1 ],
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									$(cell).html('<a href="/pa/' + rowData[0] + '" target="_blank">' + cellData + '</a>');
								} 
							},
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 2 ] },
							{
								"targets": [ 3 ],
								"className": "dt-center",
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									switch(cellData) {
										case "MA":
											$(cell).html('<div class="pa-type pa-marine">Marine</div>');
											break;
										case "TM":
											$(cell).html('<div class="pa-type pa-coastal">Coastal</div>');
											break;
										default:
											$(cell).html('<div class="pa-type pa-terrestrial">Terrestrial</div>');
									}
								} 
							}
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable(cardTwigName, tableData); 
					}
				});
				$('#collapseListPas10').on('hide.bs.collapse', function () {
					var cardTwigName = 'ListPas10'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Conservation List of protected areas ≥ 10 km2  ##
				*/
				/* 
				## Start Conservation Key Biodiversity Areas  ##
				*/
				$('#collapseKBA').on('shown.bs.collapse', function () {
					var cardTwigName = 'KBA'; //Set this once here as so there are no mistakes below.
					//
					var indicatorTitle = "Number and protection of Key Biodiversity Areas for " + currentCountry.name; 
					var tableChartColors = ['#65a70c','#e7aa27','#e36b0f','#1ba2c4'];
					
					//Datatable Simple
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [[allCountryInds.kba_n_tot, allCountryInds.kba_n_fully_prot, allCountryInds.kba_n_partially_prot, allCountryInds.kba_n_not_protected, allCountryInds.kba_avg_prot_perc]]; 
						var columnData = [ 
							{ title: "KBAs in country <br>(#)" },
            				{ title: "KBAs fully protected <br>(# > 98%)" },
							{ title: "KBAs partially protected <br>(2% < # < 98%)" }, 
							{ title: "KBAs not protected <br>(# < 2%)" },
							{ title: "KBAs average protection <br>(%)" },
						];
						var columnSettings = [
							{ "className": "dt-center "+cardTwigName+"-cell-color-0", "targets": [ 1 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-1", "targets": [ 2 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-2", "targets": [ 3 ] },
							{ "className": "dt-center "+cardTwigName+"-cell-color-3", "targets": [ 4 ] },
							{ "className": "dt-center", "targets": [ '_all' ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: false
						}
						$().createDataTable(cardTwigName, tableData); 
						$().updateCellColors(cardTwigName, tableChartColors);
					}
					
				});
				$('#collapseKBA').on('hide.bs.collapse', function () {
					var cardTwigName = 'KBA'; //Set this once here as so there are no mistakes below.
					// 
				});
				/* 
				## End Conservation Key Biodiversity Areas  ##
				*/
				/* 
				## Start Conservation Estimated number of threatened and near threatened species for protected areas  ##
				*/
				$('#collapseEstPaSpeciesThreat').on('shown.bs.collapse', function () {
					var cardTwigName = 'EstPaSpeciesThreat'; //Set this once here as so there are no mistakes below.
					//
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$(currentTableDiv).append(miniLoadingSpinner);
					var indicatorTitle = "Estimated number of threatened and near threatened species\nin protected areas for " + currentCountry.name; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					var paNumbersUrl = DopaBaseUrl+'species/get_country_pa_redlist_th_count?format=json&b_iso2=' + currentCountry.iso2;
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						$.getJSON(paNumbersUrl,function(d){
							var dataSet = [];
							var chartXaxisData = [];
							var chartSeriesData = [];
							$(d.records).each(function(i, data) {
								var newDataRow = [];
								if (data.count_id == "-999") data.count_id = 0; 
								newDataRow.push(data.wdpaid, data.name, data.count_id);
								dataSet.push(newDataRow);
								chartXaxisData.push(data.name);
								chartSeriesData.push(data.count_id);
							});
							//Chart
							var chartData = {
								colors: tableChartColors,
								title: indicatorTitle,
								yAxisTitle: "Number (#)",
								xAxis: {
									type: 'category',
									data: chartXaxisData
								},
								series: [{
								  data: chartSeriesData,
								  type: 'bar',
								}] 
							}
							$().createXYAxisChart(cardTwigName , chartData); 
							var columnData = [
								{ title: "WDPA ID" },
								{ title: "Name" },
								{ title: "Estimated number of threatened and near threatened species" },
							];
							//<a href="/wdpa/555563545" target="_blank">91 Ammunisie Depo Private Nature Reserve</a>
							var columnSettings = [
								{ "visible": false, "targets": [ 0 ] },
								{
									"targets": [ 1 ],
									"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
										$(cell).html('<a href="/pa/' + rowData[0] + '" target="_blank">' + cellData + '</a>');
									} 
								},
								{ "className": "dt-center", "targets": [ 2 ] },
							];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								defaultSort: [[ 2, "desc" ]],
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true
							}
							$().createDataTable(cardTwigName, tableData); 
						});
					}
				});
				$('#collapseEstPaSpeciesThreat').on('hide.bs.collapse', function () {
					var cardTwigName = 'EstPaSpeciesThreat'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Conservation Estimated number of threatened and near threatened species for protected areas ##
				*/
				/* 
				## Start Conservation Habitat diversity in protected areas ≥ 10 km2  ##
				*/
				$('#collapsePaHabitatDiversity10').on('shown.bs.collapse', function () {
					var cardTwigName = 'PaHabitatDiversity10'; //Set this once here as so there are no mistakes below.
					//
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$(currentTableDiv).append(miniLoadingSpinner);
					var indicatorTitle = "Terrestrial and Marine Habitat Diversity Indicator for Protected Areas\nbigger than 10km in " + currentCountry.name; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [];
						var chartXaxisData = [];
						var chartSeriesData = [];
						var sortedHdi = allCountryPaStats.sort( $().sortObject("hdi", "dec") );
						$(sortedHdi).each(function(i, data) {
							var newDataRow = [];
							if (data.hdi == "-999") data.hdi = 0; 
							newDataRow.push(data.wdpaid, data.name, data.hdi);
							dataSet.push(newDataRow);
							chartXaxisData.push(data.name);
							chartSeriesData.push(data.hdi);
						});
						//Chart
						var chartData = {
							colors: tableChartColors,
							title: indicatorTitle,
							yAxisTitle: "Number (#)",
							xAxis: {
								type: 'category',
								data: chartXaxisData,
								title: "Protected Area Name"
							},
							series: [{
							  data: chartSeriesData,
							  type: 'bar',
							}] 
						}
						$().createXYAxisChart(cardTwigName , chartData); 
						var columnData = [
							{ title: "WDPA ID" },
							{ title: "Protected Area Name" },
							{ title: "Habitat diversity" },
						];
						//<a href="/wdpa/555563545" target="_blank">91 Ammunisie Depo Private Nature Reserve</a>
						var columnSettings = [
							{ "visible": false, "targets": [ 0 ] },
							{
								"targets": [ 1 ],
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									$(cell).html('<a href="/pa/' + rowData[0] + '" target="_blank">' + cellData + '</a>');
								} 
							},
							{ "className": "dt-center", "targets": [ 2 ] },
						];
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							defaultSort: [[ 2, "desc" ]],
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable(cardTwigName, tableData); 
					}
				});
				$('#collapsePaHabitatDiversity10').on('hide.bs.collapse', function () {
					var cardTwigName = 'PaHabitatDiversity10'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Conservation Habitat diversity in protected areas ≥ 10 km2 ##
				*/
				/* 
				## Start Pressures List of protected areas ≥ 10 km2 and associated pressures  ##
				*/
				$('#collapsePasPressures10').on('shown.bs.collapse', function () {
					var cardTwigName = 'PasPressures10'; //Set this once here as so there are no mistakes below.
					//
					var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
					$(currentTableDiv).append(miniLoadingSpinner);
					var indicatorTitle = "List of protected areas ≥ 10 km2 and associated pressures\nin " + currentCountry.name; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var dataSet = [];
						$(allCountryPaStats).each(function(i, data) {
							var newDataRow = [];
							newDataRow.push(data.wdpaid, data.name, data.gis_area, data.ap, data.rp_in, data.pi, data.pop_percent_change, data.nature);
							dataSet.push(newDataRow);
						});
						var columnData = [
							{ title: "WDPA ID" },
							{ title: "Protected Area Name" },
							{ title: "Area (km\u00B2)" },
							{ title: "Agricultural Pressure" },
							{ title: "Road Pressure" },
							{ title: "Population Pressure" },
							{ title: "Population Density Change 2000-2015 (%)" },
							{ title: "Type" },
						];
						//<a href="/wdpa/555563545" target="_blank">91 Ammunisie Depo Private Nature Reserve</a>
						var columnSettings = [
							{ "visible": false, "targets": [ 0 ] },
							{
								"targets": [ 1 ],
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									$(cell).html('<a href="/pa/' + rowData[0] + '" target="_blank">' + cellData + '</a>');
								} 
							},
							{
								"targets": [ 7 ],
								"createdCell": function (cell, cellData, rowData, rowIndex, colIndex) {
									switch(cellData) {
										case "MA":
											$(cell).html('<div class="pa-type pa-marine">Marine</div>');
											break;
										case "TM":
											$(cell).html('<div class="pa-type pa-coastal">Coastal</div>');
											break;
										default:
											$(cell).html('<div class="pa-type pa-terrestrial">Terrestrial</div>');
									}
								} 
							},
							{ "className": "dt-center", "targets": [ 2,3,4,5,6,7 ] },
						]; 
						var tableData = {
							title: indicatorTitle,
							columns: columnData,
							defaultSort: [[ 2, "desc" ]],
							columnDefs: columnSettings,
							data: dataSet,
							attribution: dopaAttribution,
							isComplex: true
						}
						$().createDataTable(cardTwigName, tableData); 
					}
				});
				$('#collapsePasPressures10').on('hide.bs.collapse', function () {
					var cardTwigName = 'PasPressures10'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Pressures List of protected areas ≥ 10 km2 and associated pressures ##
				*/
				/* 
				## Start Pressures Working poor at $3.10 a day (% of total employment)  ##
				*/
				$('#collapseWorkingPoor').on('shown.bs.collapse', function () {
					var cardTwigName = 'WorkingPoor'; //Set this once here as so there are no mistakes below.
					var indicatorTitle = "Working poor at $3.10 a day (% of total employment)\nin " + currentCountry.name; 
					var tableChartColors = ['rgb(127, 173, 65)'];
					//
					
					if (!$('#collapse'+cardTwigName).hasClass('indicatorProccessed')){
						var currentTableDiv = "#" + $(this).find('table').attr('id'); //needed so we can place the spinner where it needs to go
						$(currentTableDiv).append(miniLoadingSpinner);
						//https://dopa-services.jrc.ec.europa.eu/services/d6dopa40/administrative_units/get_country_ecoregions_stats?format=json&c_un_m49=516
						var workingPoorUrl = 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=dopa_explorer_3:undp_poverty_employed_people&maxFeatures=50&outputFormat=application%2Fjson&propertyName=a_1991,a_1992,a_1993,a_1994,a_1995,a_2000,a_2005,a_2010,a_2011,a_2012,a_2013,mean,HDI_Rank&CQL_FILTER=un_m49='+currentCountry.un;
						$.getJSON(workingPoorUrl,function(d){
							var data = d.features[0].properties
							var dataSet = [["1991", data.a_1991], ["1992", data.a_1992], ["1993", data.a_1993], ["1994", data.a_1994], ["1995", data.a_1995], ["2000", data.a_2000], ["2005", data.a_2005], ["2010", data.a_2010], ["2011", data.a_2011], ["2012", data.a_2012], ["2013", data.a_2013]];
							var chartXaxisData = ["1991", "1992", "1993", "1994", "1995", "2000", "2005", "2010", "2011", "2012", "2013"];
							var chartSeriesData = [data.a_1991, data.a_1992, data.a_1993, data.a_1994, data.a_1995, data.a_2000, data.a_2005, data.a_2010, data.a_2011, data.a_2012, data.a_2013];
							//Chart
							var chartData = {
								colors: tableChartColors,
								title: indicatorTitle,
								yAxisTitle: "Percent (%)",
								xAxis: {
									type: 'category',
									data: chartXaxisData,
									title: "Year" 
								},
								series: [{
								  data: chartSeriesData,
								  type: 'bar',
								  markLine: {
									  data: [{
										type: "average",
										name: 'Average',
									  }],
									  label: {
										show: true,
										position: "insideEndTop",
										formatter: '{b}: {c}%',
									  }
								  },
								}] 
							}
							$().createXYAxisChart(cardTwigName , chartData); 
							var columnData = [
								{title: "Year"},
								{title: "Working Poor (%)"},
							];
							var columnSettings = [{ "className": "dt-center", "targets": [ '_all' ] }];
							var tableData = {
								title: indicatorTitle,
								columns: columnData,
								columnDefs: columnSettings,
								data: dataSet,
								attribution: dopaAttribution,
								isComplex: true
							}
							$().createDataTable(cardTwigName, tableData);
						});
					}
				});
				$('#collapseWorkingPoor').on('hide.bs.collapse', function () {
					var cardTwigName = 'WorkingPoor'; //Set this once here as so there are no mistakes below.
					//
				});
				/* 
				## End Pressures Working poor at $3.10 a day (% of total employment) ##
				*/
		    });
		}
    };
})(jQuery, Drupal);


var _print = window.print;

(function ($, Drupal) {
	Drupal.behaviors.biopamaReport = {
		attach: function (context, settings) {
		    $(window).once().on('load', function () {

				
				var reportIndicators = $.jStorage.get("country-report");
				console.log(reportIndicators);
				var indicatorIconRow = "<div class='indicator-move-handle'>&nbsp;&nbsp;<i class='fas fa-arrows-alt'></i></div>"
				for (const indicator in reportIndicators) {
					if (reportIndicators[indicator].type == "table"){ 
						$( "#sortable-bin" ).append( "<li class='indicator'>"+ 
							"<h5 class='card-title d-flex justify-content-between' id='card-title-" + indicator + "' type='button' data-toggle='collapse' data-target='#collapse" + indicator + "' aria-expanded='false' aria-controls='" + indicator + "'><i class='fas fa-table'>&nbsp;&nbsp;</i><div class='inner-card-title'>"+reportIndicators[indicator].title+"</div><div class='data-collapsed'><i class='fas fa-toggle-off'></i></div><div class='data-expanded'><i class='fas fa-toggle-on'></i></div>"+indicatorIconRow+"</h5>"+
							"<div class='collapse wrapper-data-card' id='collapse" + indicator + "'>" +
								"<table class='dopa-data-table' id='dopa-data-"+reportIndicators[indicator].type+"-"+indicator+"'></table>" +
							"</div>"+
						"</li>");
					} else {
						/* $( "#sortable-bin" ).append( "<li class='indicator'><i class='far fa-chart-bar'></i> " + reportIndicators[indicator].title + "<br>"+
						"<div class='dopa-data-chart' id='dopa-data-"+reportIndicators[indicator].type+"-"+indicator+"'></div></li>" ); */
						$( "#sortable-bin" ).append( "<li class='indicator'>"+
							"<h5 class='card-title d-flex justify-content-between' id='card-title-" + indicator + "' type='button' data-toggle='collapse' data-target='#collapse" + indicator + "' aria-expanded='false' aria-controls='" + indicator + "'><i class='far fa-chart-bar'>&nbsp;&nbsp;</i><div class='inner-card-title'>"+reportIndicators[indicator].title+"</div><div class='data-collapsed'><i class='fas fa-toggle-off'></i></div><div class='data-expanded'><i class='fas fa-toggle-on'></i></div>"+indicatorIconRow+"</h5>"+ 
							"<div class='collapse wrapper-data-card' id='collapse" + indicator + "'>" +
								"<div class='dopa-data-chart' id='dopa-data-"+reportIndicators[indicator].type+"-"+indicator+"'>"+
							"</div>"+
						"</li>");
						
					}
					
					$('body').addClass('loaded'); 
					
				}
				
				//$('.collapse').collapse();
				
				$('.wrapper-data-card').on('shown.bs.collapse', function () {
					var indicator = $(this).attr("id").replace("collapse", "");	 
					if (reportIndicators[indicator].type == "table"){ 
						$().createReportDataTable(indicator, reportIndicators[indicator]); 
					} else {
						if (reportIndicators[indicator].chartType == "NoAxis"){ 
							$().createNoAxisChart(indicator, reportIndicators[indicator], true);
						} else {
							$().createXYAxisChart(indicator , reportIndicators[indicator], true);  
						}
					}
				});
				
				
				$( function() {
					$( "#sortable-bin, #sortable-report" ).sortable({
						connectWith: ".connectedSortable",
						revert: true
					}).disableSelection();
				});
				$( "#draggable" ).draggable({
					connectToSortable: "#sortable-bin, #sortable-report",
					helper: "clone",
					scroll: false,
					revert: "invalid",
					delay: 200,
					containment: "#report-area",
					opacity: 0.35,
				});
				$( "ul, li" ).disableSelection();
				
				window.print = function() {
					$("#report-print-button").find('i').toggleClass('fa-print fa-spinner').addClass("fa-spin");
				  	$('#sortable-report .collapse, #collapseHeader, #collapseFooter').collapse('show');
				  	setTimeout(function(){
						_print(); 
						$("#report-print-button").find('i').toggleClass('fa-spinner fa-print').removeClass("fa-spin");
					}, 1500); //need to wait for BOTH the expand event and the chart draw event (~1500ms)
				  
				}
			

				
			});
		}
    };		
})(jQuery, Drupal);
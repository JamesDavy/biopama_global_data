<?php

/* 
Anatomy of a Data Card
'cardId' => [ required to identify and use the card in JS
  'name' => '',
  'description' => '', 
  'descriptionLong' => '', //not really used, but it could be!
  'customTop' => '', //Puts a generic div at the top for more custom content - for injecting via JS
  'chart' => BOOL,
  'table' => BOOL, 
  'layers' => [[
    'name' => '', //title of the layer
	'id' => '', //ID to uniquely manage this layer in the map !Do not include an ID if there is no layer! 
	'buttonTitle' => '', //if there's more then one layer in the array this will be in the title of the generated button
	'type' => '', //Raster or Vector
	'scheme' => '', //if raster - xyz, tms... other? 
	'url' => '', //the URL - it it needs an argument, that's managed in JS
	'customArgs' => BOOL, //setting this to true will prevent the layer from being rendered normally. Instead it will need to be called specifically so we can attach the custom arguments.
	'description' => '', 
	'legend' => [
	  'type' => 'choropleth', //only choropleth or gradient
	  'classes' => [
	  	[
		  'color' => '#fff', //supports any color type (hex, rgb, etc)
		  'label' => '', //it's what is written under the corresponding color 
		  'class' => 'fires_1d', //optional - todo add interaction
		]
	  ]
	], 
  ]],
  'customBottom' => '', //Puts a generic div at the bottom for more custom content - for injecting via JS
  'link' => '', //!mandatory
  'linkIcon' => '', //todo - add optional icon for reference link
  'linkText' => '', //if null, it will display default text
],
 */

$DataCardsHappeningNow = [ 
    'Fires' => [
		'name' => 'Fires',
		'description' => 'Active fire products from the Moderate Resolution Imaging Spectroradiometer (MODIS) for the last 24 hours up to the last 90 days.', 
		'layers' => [[
			'name' => 'Active Fires',
			'buttonTitle' => '1 day',
			'id' => 'fires_1d', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/https://ies-ows.jrc.ec.europa.eu/gwis?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=modis.hs&zIndex=72&opacity=1&time=', //needs time in format start/end 'YYYY-MM-DD/YYYY-MM-DD'
			'customArgs' => TRUE,
			'description' => 'Active fires are located on the basis of the so-called thermal anomalies produced by them. The algorithms compare the temperature of a potential fire with the temperature of the land cover around it; if the difference in temperature is above a given threshold, the potential fire is confirmed as an active fire or "hot spot".', 
			'legend' => [
				'type' => 'choropleth', //only choropleth or gradient
				'classes' => [
					[
						'color' => '#f00', 
						'label' => 'Last 1 Day',
						'class' => 'fires_1d',
					],
				],
			],
		],
		[
			'name' => 'Active Fires',
			'buttonTitle' => '7 days',
			'id' => 'fires_7d', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/https://ies-ows.jrc.ec.europa.eu/gwis?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=modis.hs&zIndex=72&opacity=1&time=', //needs time in format start/end 'YYYY-MM-DD/YYYY-MM-DD'
			'customArgs' => TRUE,
			'description' => 'Active fires are located on the basis of the so-called thermal anomalies produced by them. The algorithms compare the temperature of a potential fire with the temperature of the land cover around it; if the difference in temperature is above a given threshold, the potential fire is confirmed as an active fire or "hot spot".', 
			'legend' => [
				'type' => 'choropleth', //only choropleth or gradient
				'classes' => [
					[
						'color' => '#f00', 
						'label' => 'Last 1 Day',
						'class' => 'fires_1d',
					],
					[
						'color' => '#ff7f00', 
						'label' => 'Last 7 Days',
						'class' => 'fires_7d',
					],
				],
			],
		],[
			'name' => 'Active Fires',
			'buttonTitle' => '30 days',
			'id' => 'fires_30d', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/https://ies-ows.jrc.ec.europa.eu/gwis?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=modis.hs&zIndex=72&opacity=1&time=', //needs time in format start/end 'YYYY-MM-DD/YYYY-MM-DD'
			'customArgs' => TRUE,
			'description' => 'Active fires are located on the basis of the so-called thermal anomalies produced by them. The algorithms compare the temperature of a potential fire with the temperature of the land cover around it; if the difference in temperature is above a given threshold, the potential fire is confirmed as an active fire or "hot spot".', 
			'legend' => [
				'type' => 'choropleth', //only choropleth or gradient
				'classes' => [
					[
						'color' => '#f00', 
						'label' => 'Last 1 Day',
						'class' => 'fires_1d',
					],
					[
						'color' => '#ff7f00', 
						'label' => 'Last 7 Days',
						'class' => 'fires_7d',
					],
					[
						'color' => '#1e90ff', 
						'label' => 'Last 30 Days',
						'class' => 'fires_30d',
					],
				],
			],
		],[
			'name' => 'Active Fires',
			'buttonTitle' => '90 days',
			'id' => 'fires_90d', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/https://ies-ows.jrc.ec.europa.eu/gwis?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.1.1&request=GetMap&srs=EPSG:3857&transparent=true&width=256&height=256&layers=modis.hs&zIndex=72&opacity=1&time=', //needs time in format start/end 'YYYY-MM-DD/YYYY-MM-DD'
			'customArgs' => TRUE,
			'description' => 'Active fires are located on the basis of the so-called thermal anomalies produced by them. The algorithms compare the temperature of a potential fire with the temperature of the land cover around it; if the difference in temperature is above a given threshold, the potential fire is confirmed as an active fire or "hot spot".', 
			'legend' => [
				'type' => 'choropleth', //only choropleth or gradient
				'classes' => [
					[
						'color' => '#f00', 
						'label' => 'Last 1 Day',
						'class' => 'fires_1d',
					],
					[
						'color' => '#ff7f00', 
						'label' => 'Last 7 Days',
						'class' => 'fires_7d',
					],
					[
						'color' => '#1e90ff', 
						'label' => 'Last 30 Days',
						'class' => 'fires_30d',
					],
					[
						'color' => '#588d0b', 
						'label' => 'Last 90 Days',
						'class' => 'fires_90d',
					],
				],
			],
		]],
		'link' => 'https://earthdata.nasa.gov/earth-observation-data/near-real-time/firms/about-firms', 
		'linkText' => 'Get more info about Active Fires', 
    ],
    'Floods' => [ 
		'name' => 'Floods',
		'description' => 'Global historical and current flood events derived from news, governmental, instrumental, and remote sensing sources from the Dartmouth Flood Observatory and Flood hazard 100 year return period Layer from <a href="http://www.globalfloods.eu/">Global Flood Awareness System</a>',  
		'buttonGroup' => NULL, 
		'layers' => [[
			'name' => 'Flood hazard 100 year return period',
			'id' => 'floods', 
			'type' => 'raster',
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/http://globalfloods-ows.ecmwf.int/glofas-ows/ows.py?bbox={bbox-epsg-3857}&format=image/png&service=WMS&version=1.3.0&request=GetMap&crs=EPSG:3857&transparent=true&width=256&height=256&layers=FloodHazard100y&zIndex=33&opacity=1&time=', //YYYY-MM-DD
			'description' => 'Inundated areas for flood events with a return period of 100 years, based on GloFAS climatology. Permanent water bodies derived from the Global Lakes and Wetlands Database and from the Natural Earth lakes map (<a href="http://www.naturalearthdata.com" target="_blank">naturalearthdata.com</a>).', 
			'legend' => [
				'type' => 'choropleth',
				'classes' => [	  
					[
						'color' => '#b8dbff', 
						'label' => 'Shallow (less than 1m)',
						'class' => 'floods_ne_05m',
					],
					[
						'color' => '#99ccff', 
						'label' => 'Moderate (between 1 and 3 m)',
						'class' => 'floods_ne_1m',
					],
					[
						'color' => '#6699ff', 
						'label' => 'Deep (between 3 and 10 m)',
						'class' => 'floods_ne_10m',
					],
					[
						'color' => '#3366ff', 
						'label' => 'Very deep (permanent water)',
						'class' => 'floods_ne_05m',
					],
				],
			],
		]], 
		'link' => 'http://floodobservatory.colorado.edu/', 
		'linkText' => 'Get more info about Flood Events', 
    ],
/*     'Droughts' => [
		'name' => 'Droughts',
		'description' => 'The indicator shows the risk of having impacts from a drought, by taking into account the exposure and socio-economic vulnerability of the area, with particular focus on the agricultural impacts.', 
		'buttonGroup' => NULL, 
		'layers' => [[
			'name' => 'Risk of Drought Impact',
			'id' => 'drought', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/https://edo.jrc.ec.europa.eu/gdo/php/gis/mswms.php?bbox={bbox-epsg-3857}&map=gdo_w_mf&width=256&height=256&SRS=EPSG%3A3857&LAYERS=rdri_png%2Cgrid_1dd_rdri&FORMAT=image%2Fpng&TRANSPARENT=TRUE&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&', //SELECTED_YEAR=YYY&SELECTED_MONTH=MM&SELECTED_TENDAYS=21
			'description' => 'The indicator shows the risk of having impacts from a drought, by taking into account the exposure and socio-economic vulnerability of the area, with particular focus on the agricultural impacts. Formerly known as Likelihood of Drought Impact (LDI), it differs from the latter in that soil moisture anomaly is now included and updated every ten days (dekad).',
			'legend' => [
			'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#feea08', 
						'label' => 'Low',
						'class' => 'edo_10m',
					],
					[
						'color' => '#ff7f00', 
						'label' => 'Medium',
						'class' => 'edo_1m',
					],
					[
						'color' => '#f00', 
						'label' => 'High',
						'class' => 'edo_05m',
					],
				],
			],
		]],  
		'link' => 'http://edo.jrc.ec.europa.eu/gdo/php/index.php?id=2000', 
		'linkText' => "Get more info about Risk of Droughts", 
    ], */
	'SST' => [
		'name' => 'Sea Surface Temperature Anomalies',
		'description' => 'The NOAA Coral Reef Watch (CRW) twice-weekly 50-km Sea Surface Temperature (SST) Anomaly product displays the difference between today\'s SST and the long-term average. The scale goes from -5 to +5 °C.', 
		'buttonGroup' => NULL, 
		'layers' => [[
			'name' => 'Sea Surface Temperature Anomalies',
			'id' => 'sst', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://proxy.biopama.org/https://coralreefwatch.noaa.gov/data1/vs/google_maps/ssta/ssta_gm_tiles/{z}/{x}/{y}.png', //SELECTED_YEAR=YYY&SELECTED_MONTH=MM&SELECTED_TENDAYS=21
			'description' => 'The NOAA Coral Reef Watch (CRW) twice-weekly 50-km Sea Surface Temperature (SST) Anomaly product displays the difference between today\'s SST and the long-term average. The scale goes from -5 to +5 °C. Positive numbers mean the temperature is warmer than average; negative means cooler than average. More at <a href="https://coralreefwatch.noaa.gov/satellite/education/tutorial/crw25_sba_product.php">NOAA Website</a><b> Please zoom out to see the layer</b>',
			'legend' => [
			'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#65005b', 
						'label' => '-5',
					],
					[
						'color' => '#3e1ea5', 
						'label' => '-4',
					],
					[
						'color' => '#0018c7', 
						'label' => '-3',
					],
					[
						'color' => '#0058ff', 
						'label' => '-2',
					],
					[
						'color' => '#00cdff', 
						'label' => '-1',
					],
					[
						'color' => '#dcdcdc', 
						'label' => '0',
					],
					[
						'color' => '#fbdc00', 
						'label' => '1',
					],
					[
						'color' => '#f0a000', 
						'label' => '2',
					],
					[
						'color' => '#f56400', 
						'label' => '3',
					],
					[
						'color' => '#ef1e00', 
						'label' => '4',
					],
					[
						'color' => '#870c00', 
						'label' => '5', 
					],
				],
			],
		]],  
		'link' => 'https://coralreefwatch.noaa.gov/satellite/education/tutorial/crw20_ssta_product.php', 
    ],
	'CoralBleaching' => [
		'name' => 'Coral Bleaching Heat Stress Alerts',
		'description' => 'The NOAA Coral Reef Watch (CRW) twice-weekly 50-km Sea Surface Temperature (SST) Anomaly product displays the difference between today\'s SST and the long-term average. The scale goes from -5 to +5 °C.', 
		'buttonGroup' => NULL, 
		'layers' => [[
			'name' => 'Coral Bleaching Heat Stress Alerts',
			'id' => 'coralBleaching', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://proxy.biopama.org/https://coralreefwatch.noaa.gov/data1/vs/google_maps/ssta/ssta_gm_tiles/{z}/{x}/{y}.png', //SELECTED_YEAR=YYY&SELECTED_MONTH=MM&SELECTED_TENDAYS=21
			'description' => 'Level of stress of the Global Coral Reefs derived from NOAA Alerts Bleaching Alerts. More at: <a href="https://coralreefwatch.noaa.gov/satellite/education/tutorial/crw25_sba_product.php">NOAA Website</a><b> Please zoom out to see the layer</b>',
			'legend' => [
			'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#ffffff', 
						'label' => 'No Stress',
					],
					[
						'color' => '#faf300', 
						'label' => 'Watch',
					],
					[
						'color' => '#ec3747', 
						'label' => 'Alert Level 1',
					],
					[
						'color' => '#990009',  
						'label' => 'Alert Level 2',
					],
				],
			],
		]],  
		'link' => 'https://coralreefwatch.noaa.gov/satellite/education/tutorial/crw25_sba_product.php', 
    ],
];

$DataCardsClimate = [
    'ElevationProfile' => [
		'name' => 'Elevation profile',
		'description' => 'Virtual elevation profile of the country providing minimum, maximum, median, mean elevation values in meters.', 
		'chart' => TRUE,
		'table' => TRUE, 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Elevation', 
    ],
];

$DataCardsEcosystems = [
    'TerrestrialEcoregions' => [
		'name' => 'Terrestrial ecoregions',
		'description' => 'List of terrestrial ecoregions in country and protected areas coverage statistics.', 
		'buttonGroup' => NULL, 
		'chart' => NULL,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Gap of Protection towards Aichi Target 11',
			'description' => NULL,
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#7fad41', 
						'label' => 'On target', 
					],
					[
						'color' => '#b3b536', 
						'label' => '-1%',
					],
					[
						'color' => '#b4b536', 
						'label' => '-2%',
					],
					[
						'color' => '#e49827', 
						'label' => '-5%',
					],[
						'color' => '#e46327', 
						'label' => '-10%',
					],[
						'color' => '#fd1300', 
						'label' => '-17%',
					],
				],
			],
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Terrestrial+Coverage+by+Protected+Areas', 
    ],
	'MarineEcoregions' => [
		'name' => 'Marine ecoregions',
		'description' => 'List of marine ecoregions in country and protected areas coverage statistics.', 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => NULL,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Gap of Protection towards Aichi Target 11',
			'description' => NULL,
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#7fad41', 
						'label' => 'On target',
					],
					[
						'color' => '#b3b536', 
						'label' => '-1%',
					],
					[
						'color' => '#e49827', 
						'label' => '-2%',
					],[
						'color' => '#e46327', 
						'label' => '-5%',
					],[
						'color' => '#fd1300', 
						'label' => '-10%',
					],
				],
			],
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Marine+Coverage+by+Protected+Areas',  
    ],
	'InlandSurfaceWater' => [ 
		'name' => 'Inland surface water',
		'description' => 'Areas of permanent and seasonal surface inland water and their changes over time (1984 - 2018) are expressed in km<sup>2</sup> and percentages.', 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Water Occurrence (1984-2018)',
			'id' => 'waterOccurrence', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://proxy.biopama.org/https://cidportal.jrc.ec.europa.eu/jeodpp/services/ows/ts/hydrography/gsw-jrc?z={z}&x={x}&y={y}&format=png&layers=HY.GSW.Occurrence', 
			'description' => 'The Water Occurrence dataset shows where surface water occurred between 1984 and 2018 and provides information concerning overall water dynamics. This product captures both the intra and inter-annual variability and changes. The occurrence is a measurement of the water presence frequency (expressed as a percentage of the available observations over time actually identified as water). The provided occurrence accommodates for variations in data acquisition over time (i.e. temporal deepness and frequency density of the satellite observations) in order to provide a consistent characterization of the water dynamic over time.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#fff', 
						'label' => 'Sometimes Water ( >0% )',
					],
					[
						'color' => '#bf7fbf', 
						'label' => '',
					],
					[
						'color' => 'blue', 
						'label' => 'Always Water ( 100% )',
					],
				],
			],
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Inland+Surface+Water', 
    ],
	'ForestCover' => [
		'name' => 'Forest cover',
		'description' => 'Forest cover (2000), forest loss (2000-2018) and forest gain (2000-2012) statistics are expressed in km<sup>2</sup> and percent of the country area. Maps with the location of the areas of forest gain and loss are also provided.', 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Forest Loss and Gain',
			'id' => 'ForestLossGain', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://storage.googleapis.com/earthenginepartners-hansen/tiles/gfc_v1.4/loss_alpha/{z}/{x}/{y}.png', 
			'description' => 'Forest loss/gain layer as derived from a global remote sensing product based on Landsat data. See <a href="http://www.globalforestwatch.org/" target="_blank">http://www.globalforestwatch.org/</a> for more details',
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => 'red', 
						'label' => 'Forest Loss',
					],
					[
						'color' => 'blue', 
						'label' => 'Forest Gain',
					],
				],
			],
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Forest+Cover', 
    ],
	'LandDegradation' => [
		'name' => 'Land degradation',
		'description' => 'Virtual elevation profile of the country providing minimum, maximum, median, mean elevation values in meters.', 
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Land Productivity', 
			'id' => 'LandProductivity', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3%3ALPD&STYLES=&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&SRS=EPSG%3A3857&BBOX={bbox-epsg-3857}', 
			'description' => NULL,
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#ab2828', 
						'label' => 'Persistent severe decline in productivity',
					],
					[
						'color' => '#ed7325', 
						'label' => 'Persistent moderate decline in productivity',
					],
					[
						'color' => '#ffd954', 
						'label' => 'Stable, but stressed',
					],
					[
						'color' => '#b8d879', 
						'label' => 'Stable productivity',
					],
					[
						'color' => '#46a246', 
						'label' => 'Persistent increase in productivity',
					],
				], 
			],
		]],
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Productivity', 
    ], 
	'LandFragmentation' => [
		'name' => 'Land fragmentation',
		'description' => 'Virtual elevation profile of the country providing minimum, maximum, median, mean elevation values in meters.', 
		'descriptionLong' => NULL, 
		'buttonGroup' => NULL, 
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Land Fragmentation',
			'id' => 'LandProductivity', 
			'type' => 'raster', 
			'scheme' => 'xyz',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3%3Aland_fragmentation&STYLES=&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&SRS=EPSG%3A3857&BBOX={bbox-epsg-3857}&TIME=1995-01-01', 
			'description' => NULL,
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#7fad41', 
						'label' => 'Core',
					],
					[
						'color' => '#878787', 
						'label' => 'Non Natural',
					],
					[
						'color' => '#52781f', 
						'label' => 'Edge',
					],
					[
						'color' => '#f1c200', 
						'label' => 'Core Perforation',
					],
					[
						'color' => '#e89717', 
						'label' => 'Islet',
					],
					[
						'color' => '#d56317', 
						'label' => 'Linear',
					],
				], 
			],
		]], 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Fragmentation', 
    ],
];
$DataCardsEcosystemServices = [
    'BelowGroundCarbon' => [
		'name' => 'Below ground carbon',
		'description' => 'Country statistics for the amount of below ground carbon', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Below Ground Carbon',
			'id' => 'bgCarbon', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:below_ground_carbon@EPSG:900913@png/{z}/{x}/{y}.png', 
			'description' => NULL,
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => '0 Mg',
					],
					[
						'color' => '#feedaa', 
						'label' => '',
					],
					[
						'color' => '#1a9641', 
						'label' => '>8,000 Mg',
					],
				],
			],
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Below+Ground+Carbon', 
		'linkText' => NULL, 
    ],
	'SoilOrganicCarbon' => [
		'name' => 'Soil organic carbon',
		'description' => 'Country statistics for the amount of soil organic carbon (0-30 cm depth)',
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Global Soil Organic Carbon (GSOC)',
			'id' => 'GSOCarbon', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:soil_organic_carbon@EPSG:900913@png/{z}/{x}/{y}.png', 
			'description' => 'The global soil organic carbon concentration map provides users with essential information on degraded areas and soil fertility as well as on the contribution to carbon storage mitigating climate change.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => '0 Mg',
					],
					[
						'color' => '#feedaa', 
						'label' => '',
					],
					[
						'color' => '#1a9641', 
						'label' => '>25,000 Mg',
					],
				],
			],
		]],  
		'link' => 'http://dopa.jrc.ec.europa.eu/sites/default/files/DOPA%20Factsheet%20J1%20Soil%20Carbon_0.pdf', 
		'linkText' => NULL, 
    ],
	'AboveGroundCarbon' => [
		'name' => 'Above ground carbon',
		'description' => 'Country statistics for above ground carbon.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Global Above Ground Carbon',
			'id' => 'AGCarbon', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:above_ground_carbon@EPSG:900913@png/{z}/{x}/{y}.png', 
			'description' => 'The above-ground carbon (AGC) layer is expressed in Mg (megagrams or tonnes) of biomass per km<sup>2</sup>.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => '0 Mg',
					],
					[
						'color' => '#feedaa', 
						'label' => '',
					],
					[
						'color' => '#1a9641', 
						'label' => '>25,000 Mg',
					],
				],
			],
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Above+Ground+Carbon', 
		'linkText' => NULL, 
    ],
	'TotalCarbon' => [
		'name' => 'Total carbon',
		'description' => 'Country statistics for the amount of total carbon (below ground C +  organic soil C + above ground C).', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Total Carbon',
			'id' => 'TCarbon', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:total_carbon@EPSG:900913@png/{z}/{x}/{y}.png', 
			'description' => NULL,
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => '1 Mg',
					],
					[
						'color' => '#feedaa', 
						'label' => '',
					],
					[
						'color' => '#1a9641', 
						'label' => '>55,000 Mg',
					],
				],
			],
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Carbon', 
		'linkText' => NULL,
    ],
];
$DataCardsLandCover = [
    'CopernicusGLC' => [
		'name' => 'Copernicus Global Land Cover 2015',
		'description' => 'Using the first aggregation level, the land cover classes are provided for this country for the year 2015 km<sup>2</sup> and %.', 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Land Cover Change (1995 to 2015)',
			'id' => 'CopernicusGLC', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:land_cover_copernicus_2018_agg_0@EPSG:900913@png/{z}/{x}/{y}.png', 
			'description' => NULL,
			'legend' => NULL
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Cover', 
		'linkText' => NULL, 
    ],
	'EsaLC' => [
		'name' => 'ESA Land Cover change 1995-2015',
		'description' => 'Using three different aggregation levels, the land cover classes are provided for this protected area for the years 1995, 2000, 2005, 2010 and 2015 in km<sup>2</sup> and %.', 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => FALSE, 
		'layers' => [[
			'name' => 'Land Cover Change (1995 to 2015)',
			'id' => 'EsaLC', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/dopa_explorer_3/wms?SERVICE=WMS&REQUEST=GetMap&VERSION=1.1.1&LAYERS=dopa_explorer_3%3ALCC_1995_2015&STYLES=&FORMAT=image%2Fpng&TRANSPARENT=true&HEIGHT=256&WIDTH=256&SRS=EPSG%3A3857&BBOX={bbox-epsg-3857}',
			'description' => NULL,
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#ff7f1f', 
						'label' => 'Natural / semi-natural land → Mosaic natural / managed land',
					],[
						'color' => '#e50003', 
						'label' => 'Natural / semi-natural land → Cultivated / managed land',
					],[
						'color' => '#3e86f3', 
						'label' => 'Natural / semi-natural land → Water / snow and ice',
					],[
						'color' => '#3cc943', 
						'label' => 'Mosaic natural / managed land → Natural / semi-natural land',
					],[
						'color' => '#fe6d70', 
						'label' => 'Mosaic natural / managed land → Cultivated / managed land',
					],[
						'color' => '#3fd6ff', 
						'label' => 'Mosaic natural / managed land → Water / snow and ice',
					],[
						'color' => '#438c28', 
						'label' => 'Cultivated / managed land → Natural / semi-natural land',
					],[
						'color' => '#c39a22', 
						'label' => 'Cultivated / managed land → Mosaic natural / managed land',
					],[
						'color' => '#00dda8', 
						'label' => 'Cultivated / managed land → Water / snow and ice',
					],[
						'color' => '#7fda95', 
						'label' => 'Water / snow and ice → Natural / semi-natural land',
					],[
						'color' => '#f4ff74', 
						'label' => 'Water / snow and ice → Mosaic natural / managed land',
					],[
						'color' => '#ffcd29', 
						'label' => 'Water / snow and ice → Cultivated / managed land', 
					],
				],
			],
		]],  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Land+Cover', 
		'linkText' => NULL, 
    ],
];
$DataCardsSpecies = [
    'SpeciesLayers' => [
		'name' => 'Species Richness',
		'description' => 'Map layers of species richness as reported by the IUCN Red List of Threatened Species',
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => FALSE, 
		'layers' => [[
			'name' => 'Amphibian Species Richness',
			'buttonTitle' => 'Amphibians',
			'id' => 'AmphibianSpeciesRichness', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:amphibians_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
			'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => 'Low richness',
					],[
						'color' => '#feedaa', 
						'label' => '',
					],[
						'color' => '#2b83ba', 
						'label' => 'High richness',
					]
				],
			],
		],[
			'name' => 'Bird Species Richness',
			'buttonTitle' => 'Birds',
			'id' => 'BirdSpeciesRichness', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:birds_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
			'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => 'Low richness',
					],[
						'color' => '#feedaa', 
						'label' => '',
					],[
						'color' => '#2b83ba', 
						'label' => 'High richness',
					]
				],
			],
		],[
			'name' => 'Mammal Species Richness',
			'buttonTitle' => 'Mammals',
			'id' => 'MammalSpeciesRichness', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:mammals_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
			'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => 'Low richness',
					],[
						'color' => '#feedaa', 
						'label' => '',
					],[
						'color' => '#2b83ba', 
						'label' => 'High richness',
					]
				],
			],
		],[
			'name' => 'Shark and Rays Species Richness',
			'buttonTitle' => 'Sharks and Rays',
			'id' => 'SharksSpeciesRichness', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:sharks_rays_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
			'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => 'Low richness',
					],[
						'color' => '#feedaa', 
						'label' => '',
					],[
						'color' => '#2b83ba', 
						'label' => 'High richness',
					]
				],
			],
		],[
			'name' => 'Coral Species Richness',
			'buttonTitle' => 'Corals',
			'id' => 'CoralsSpeciesRichness', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:corals_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
			'description' => 'Species will not necessarily be present in the protected area. The IUCN Red List of Threatened Species is complete for some groups (mammals, birds, amphibians, sharks and rays, mangroves, seagrasses, cycads, conifers, and selected marine, freshwater and invertebrate taxa), but not complete for many others (e.g., reptiles). We therefore have generated our key species indicators for the globally assessed major taxonomic groups of birds, mammals, amphibians, warm-water reef-building corals and rays & sharks only. Species ranges are mapped as generalized polygons which often include areas of unsuitable habitat, and therefore species may not occur in all of the areas where they are mapped. In general, for range-restricted taxa, ranges are mapped with a higher degree of accuracy, sometimes down to the level of individual subpopulations, compared with more widely distributed species.
Threatened = species assessed in any of the three threatened Red List categories (Critically Endangered, Endangered, Vulnerable) See http://www.iucnredlist.org/ for more details.',
			'legend' => [
				'type' => 'gradient',
				'classes' => [
					[
						'color' => '#d7191c', 
						'label' => 'Low richness',
					],[
						'color' => '#feedaa', 
						'label' => '',
					],[
						'color' => '#2b83ba', 
						'label' => 'High richness',
					]
				],
			],
		]],
		'customBottom' => '', //todo 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],
	'SpeciesAnimalPlantNumbers' => [
		'name' => 'Reported number of animal and plant species',
		'description' => 'The number of animal and plant species as reported by the IUCN Red List of Threatened Species',
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'customBottom' => '', //todo 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],
	'SpeciesThreatNumbers' => [
		'name' => 'Reported number of threatened amphibians, birds and mammals',
		'description' => 'The number of threatened, endemic and assessed amphibian, bird and mammals species as reported by the IUCN Red List of Threatened Species',
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'customBottom' => '', //todo 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],
	'SpeciesThreatEndemicNumbers' => [
		'name' => 'Reported endemic and threatened endemic vertebrates in the country',
		'description' => 'The number of threatened and endemic vertebrates as reported by the IUCN Red List of Threatened Species',
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,
		'customBottom' => '', //todo 
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ], 
	'SpeciesComputed' => [
		'name' => 'Threatened and near threatened species (computed)',
		'description' => 'The species list in country is computed from the species ranges recorded in the IUCN Red List of Threatened Species', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => FALSE,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],
	'SpeciesGbif' => [
		'name' => 'Species occurrences reported to the GBIF',
		'description' => 'Species occurrences map reported to the Global Biodiversity Information Facility (GBIF).', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => FALSE, 
		'layer' => NULL,
		'customBottom' => '<div id="wrapper-gbif-ata"></div>', //todo - everything will be added here via JS...
		'link' => 'https://www.gbif.org/', 
		'linkText' => "GBIF Home Page", 
    ],
];
$DataCardsConservation = [
    'PaProtCon' => [
		'name' => 'Protected area coverage and connectivity',
		'description' => 'This indicator assesses the protected area coverage and connectivity (ProtConn) in country expressed in km<sup>2</sup> and percentages.', 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Connectivity', 
		'linkText' => NULL, 
    ],
	'CountryAichiProt' => [
		'name' => 'Country protection relative to Aichi Target 11',
		'description' => 'This indicator represents the progress towards Aichi Target 11 of the country.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Coverage', 
		'linkText' => NULL, 
    ],
	'NumPas' => [
		'name' => 'Number of protected areas',
		'description' => 'Number of Terrestrial, Marine and Coastal protected areas in country.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Coverage', 
		'linkText' => NULL, 
    ],
	'NumPas10' => [
		'name' => 'Number of protected areas ≥ 10 km<sup>2</sup>',
		'cardId' => 'num-pas-10', 
		'description' => 'Number of Terrestrial, Marine and Coastal protected areas greater than or equal to 10 km<sup>2</sup> in country.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Coverage', 
		'linkText' => NULL, 
    ],
	'ListPas10' => [
		'name' => 'List of protected areas ≥ 10 km<sup>2</sup>',
		'cardId' => 'list-pas-10', 
		'description' => 'List of Terrestrial, Marine and Coastal protected areas greater than or equal to 10 km<sup>2</sup> in country.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Coverage', 
		'linkText' => NULL, 
    ],
	'KBA' => [
		'name' => 'Key Biodiversity Areas',
		'description' => 'Number and protection of Key Biodiversity Areas.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Key+Biodiversity+Areas', 
		'linkText' => NULL, 
    ],
	'EstPaSpeciesThreat' => [
		'name' => 'Estimated number of threatened and near threatened species for protected areas',
		'cardId' => 'est-species-threat-pas', 
		'description' => 'Estimated number of threatened and near threatened species for protected areas in country as extracted from the range maps of the species documented by the IUCN.', 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Species', 
		'linkText' => NULL, 
    ],
	'PaHabitatDiversity10' => [
		'name' => 'Habitat diversity in protected areas ≥ 10 km<sup>2</sup>',
		'cardId' => 'hab-diversity-pas-10', 
		'description' => 'Country ranking of the protected areas at least as large as 10 km<sup>2</sup> according to the diversity of their habitats.', 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Habitat', 
		'linkText' => NULL, 
    ],
];
$DataCardsPressures = [
    'PasPressures10' => [
		'name' => 'List of protected areas ≥ 10 km<sup>2</sup> and associated pressures',
		'cardId' => 'list-pas-10-pressures', 
		'description' => 'List of protected areas ≥ 10 km<sup>2</sup> and associated pressures..', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => TRUE, 
		'layer' => NULL,  
		'link' => 'https://dopa.jrc.ec.europa.eu/en/factsheet-dopa-4?title=Pressure', 
		'linkText' => NULL, 
    ],
	'WorkingPoor' => [
		'name' => 'Working poor at $3.10 a day - percent of total employment',
		'cardId' => 'working-poor', 
		'description' => 'Proportion of employed people in country who live on less than $3.10 (in Purchasing Power Parity (PPP) terms) a day, expressed as a percentage of the total employed population ages 15 and older. More at http://www.ilo.org/ilostat.', 
		'buttonGroup' => NULL,
		'chart' => TRUE,
		'table' => TRUE, 
		'layers' => [[
			'name' => 'Working poor at PPP$3.10 a day (% of total employment)',
			'id' => 'WorkingPoor', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'https://geospatial.jrc.ec.europa.eu/geoserver/gwc/service/tms/1.0.0/dopa_explorer_3:birds_richness_201901@EPSG:900913@png/{z}/{x}/{y}.png',
			'description' => 'Proportion of employed people who live on less than $3.10 (in purchasing power parity terms) a day, expressed as a percentage of the total employed population ages 15 and older. More at http://www.ilo.org/ilostat.',
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#6a8f34',
						'label' => '0-10%',
					],[
						'color' => '#96c853',  
						'label' => '10-25%',
					],[
						'color' => '#f2ce62',  
						'label' => '25-50%',
					],[
						'color' => '#e49e27',  
						'label' => '50-75%',
					],[
						'color' => '#d1771e',
						'label' => 'more than 75%',
					],
				],
			],
		]],  
		'link' => 'http://hdr.undp.org/en/indicators/153706', 
		'linkText' => NULL, 
    ],
];
$DataCardsFunding = [ 
    'Placeholder' => [
		'name' => 'Placeholder',
		'description' => 'Placeholder.', 
		'buttonGroup' => NULL,
		'chart' => FALSE,
		'table' => FALSE, 
		'layers' => [[
			'name' => 'Placeholder',
			'id' => 'Placeholder', 
			'type' => 'raster', 
			'scheme' => 'tms',
			'url' => 'Placeholder',
			'description' => 'Placeholder.',
			'legend' => [
				'type' => 'choropleth',
				'classes' => [
					[
						'color' => '#000', 
						'label' => 'Placeholder', 
					],
				],
			],
		]],  
		'link' => 'Placeholder', 
		'linkText' => "Placeholder", 
    ],
];

?>